# cdc-rdb

Relational Database utilities based on JDBC.

## Metadata analyzer
**RdbMetaAnalyzer** is a tool that analyzes meta data of a DB.  
It constructs an in-memory representation of those meta data.  
This can be save to or restored from XML.


This command-line tool has the following options:

````
USAGE
RdbMetaAnalyzer [--args-file <arg>] [--args-file-charset <arg>] [--driver
                      <arg>] [-h | -v] [--jackcess] [--no-attributes]
                      [--no-columns] [--no-data-types] [--no-foreign-keys]
                      [--no-function-columns] [--no-functions]
                      [--no-indices] [--no-primary-keys]
                      [--no-procedure-columns] [--no-procedures]
                      [--no-properties] [--no-table-types] [--no-tables]
                      [--no-user-data-types] [-pwd <arg>] [--schema <arg>]
                      --url <arg> [--user <arg>]  --xml <arg>

RdbMetaAnalyzer analyzes meta data of a database, and generates an XML
file with gathered information.
With minimum options, everything is dumped. One must pass specific options
to ignore some features.

Missing required options: url, xml

OPTIONS
    --args-file <arg>           Optional name of the file from which
                                options can be read.
                                A line is either ignored or interpreted as
                                a single argument (option or value).
                                A line is ignored when it is empty or
                                starts by any number of white spaces
                                followed by '#'.
                                A line that only contains white spaces is
                                an argument.
                                A comment starts by a '#' not following a
                                '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is
                                not the OS default file encoding.
    --driver <arg>              Optional JDBC Driver class, which must be
                                accessible in CLASSPATH.
                                Examples:
                                - net.ucanaccess.jdbc.UcanaccessDriver
                                (UCanAccess).
                                - org.apache.derby.jdbc.EmbeddedDriver
                                (Embedded Derby).
                                - org.apache.derby.jdbc.ClientDriver
                                (Server Derby).
 -h,--help                      Prints this help and exits.
    --jackcess                  Use the Jackcess analyzer for an Access
                                database instead of the provided JDBC
                                driver.
    --no-attributes             Do not generate data related to user data
                                types attributes.
    --no-columns                Do not generate data related to table
                                columns.
    --no-data-types             Do not generate data related to data
                                types.
    --no-foreign-keys           Do not generate data related to foreign
                                keys.
    --no-function-columns       Do not generate data related to function
                                columns.
    --no-functions              Do not generate data related to functions.
    --no-indices                Do not generate data related to indices.
    --no-primary-keys           Do not generate data related to primary
                                keys.
    --no-procedure-columns      Do not generate data related to procedure
                                columns.
    --no-procedures             Do not generate data related to
                                procedures.
    --no-properties             Do not generate data related to
                                properties.
    --no-table-types            Do not generate data related to table
                                types.
    --no-tables                 Do not generate data related to tables.
    --no-user-data-types        Do not generate data related to user data
                                types.
 -pwd,--password <arg>          Optional user password.
    --schema <arg>              Optional set of schemas that must be
                                analyzed. When empty, all schemas are
                                analyzed.
    --url <arg>                 Mandatory URL of the database.
                                Examples:
                                - jdbc:ucanaccess://<path_to_db_file>
                                (UCanAccess)
                                - jdbc:derby://<path_to_db_directory>
                                (Embedded Derby)
                                - jdbc:derby://<host>[:<port>]/<db_name>
                                (Server Derby)
                                - <path_to_db_file> (--jackcess option)
    --user <arg>                Optional user name.
 -v,--version                   Prints version and exits.
    --xml <arg>                 Mandatory XML output filename.
````

## Database dump
**RdbDump** is a tool that can dump tables of a DB to different formats (CSV, XLSX, XML, ...)  
It can be configured to select tables and columns that must be dumped.  
Structure of generated XML can be controlled.
DB names (tables, columns, ...) can be converted.

This command-line tool has the following options:

````
USAGE
RdbDump [--args-file <arg>] [--args-file-charset <arg>] [--config <arg>]
              [--config-init] [--converters <arg>] [--csv] [--driver
              <arg>] [--generic-elements] [-h | -v] [--no-empty-values]
              [--ods] --output <arg> [--prefix <arg>] [-pwd <arg>]
              [--schema <arg>] [--specific-attributes]
              [--specific-elements] --url <arg> [--user <arg>]
              [--verbose] [--xlsx] [--xml]

RdbDump can export tables of a database as CSV, XLSX, XML, ...
One can configure the data (tables, columns, ...) to using with an XML
config file.
A empty config file can be generated and later be customized.

Missing required options: url, output

OPTIONS
    --args-file <arg>           Optional name of the file from which
                                options can be read.
                                A line is either ignored or interpreted as
                                a single argument (option or value).
                                A line is ignored when it is empty or
                                starts by any number of white spaces
                                followed by '#'.
                                A line that only contains white spaces is
                                an argument.
                                A comment starts by a '#' not following a
                                '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is
                                not the OS default file encoding.
    --config <arg>              Optional configuration file.
                                Note: one should not use schema option
                                with this one.
    --config-init               Generates a config file that can be
                                adapted for later use with --config
                                option.
    --converters <arg>          Optional converters file.
    --csv                       Generates CSV files.
    --driver <arg>              Optional JDBC Driver class.
    --generic-elements          Use generic elements for columns. XML.
 -h,--help                      Prints this help and exits.
    --no-empty-values           Do not generate empty values. XML.
    --ods                       Generates ODS files.
    --output <arg>              Output directory.
    --prefix <arg>              Optional prefix for file names.
 -pwd,--password <arg>          Optional user password.
    --schema <arg>              Optional set of schemas that must be
                                analysed. When empty, all schemas are
                                analyzed.
                                Note: this may conflict with configuration
                                file option.
    --specific-attributes       Use specific attributes for columns. XML.
    --specific-elements         Use specific elements for columns
                                (default). XML.
    --url <arg>                 URL to access database.
    --user <arg>                Optional user name.
 -v,--version                   Prints version and exits.
    --verbose                   Be verbose.
    --xlsx                      Generates XLSX files.
    --xml                       Generate XML files.
````