package cdc.rdb.core;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public final class JdbcUtils {
    private JdbcUtils() {
    }

    public static String toString(ResultSet rs) {
        final StringBuilder builder = new StringBuilder();
        try {
            final ResultSetMetaData meta = rs.getMetaData();
            for (int col = 1; col <= meta.getColumnCount(); col++) {
                if (col > 1) {
                    builder.append(", ");
                }
                builder.append(meta.getColumnName(col));
                builder.append(" (");
                builder.append(meta.getColumnTypeName(col));
                builder.append("): ");
                builder.append(rs.getObject(col));
            }
        } catch (final SQLException e) {
            builder.append(e.getMessage());
        }

        return builder.toString();
    }
}