package cdc.rdb.core;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.Index;
import com.healthmarketscience.jackcess.PropertyMap;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.TableMetaData;

import cdc.rdb.RdbCatalog;
import cdc.rdb.RdbDatabase;
import cdc.rdb.RdbForeignKey;
import cdc.rdb.RdbIndex;
import cdc.rdb.RdbIndexType;
import cdc.rdb.RdbPrimaryKey;
import cdc.rdb.RdbSchema;
import cdc.rdb.RdbTable;
import cdc.rdb.SqlDataType;
import cdc.rdb.YesNoUnknown;

/**
 * Specialization of {@link RdbAbstractAnalyzer} based on Jackcess.
 */
public final class RdbJackcessAnalyzer extends RdbAbstractAnalyzer {
    private static final Logger LOGGER = LogManager.getLogger(RdbJackcessAnalyzer.class);
    private final File file;

    private static final String TABLE = "TABLE";
    private static final String VIEW = "VIEW";

    private RdbJackcessAnalyzer(Builder builder) {
        super(builder);
        this.file = builder.file;
    }

    public File getFile() {
        return file;
    }

    @Override
    public RdbDatabase analyze() throws IOException {
        try (final Database db = new DatabaseBuilder().setFile(file)
                                                      .setReadOnly(true)
                                                      .open()) {
            final RdbDatabase rdb = RdbDatabase.builder().name(file.getPath()).build();

            if (!isEnabled(Hint.NO_TABLE_TYPES)) {
                analyzeTableTypes(db, rdb);
            }

            if (!isEnabled(Hint.NO_TABLES)) {
                analyzeSystemTables(db, rdb);
                analyzeTables(db, rdb);
            }

            return rdb;
        }
    }

    private static String getDescription(PropertyMap props) {
        final PropertyMap.Property prop = props.get(PropertyMap.DESCRIPTION_PROP);
        return prop == null
                ? null
                : (String) prop.getValue();
    }

    private static String getDefaultValue(PropertyMap props) {
        final PropertyMap.Property prop = props.get(PropertyMap.DEFAULT_VALUE_PROP);
        return prop == null || prop.getValue() == null
                ? null
                : prop.getValue().toString();
    }

    private static Boolean negate(Boolean b) {
        if (b == null) {
            return null;
        } else if (b.booleanValue()) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    private static Boolean isNullable(PropertyMap props) {
        final PropertyMap.Property prop = props.get(PropertyMap.REQUIRED_PROP);
        return prop == null || prop.getValue() == null
                ? null
                : negate((Boolean) prop.getValue());
    }

    private static SqlDataType getSqlDataType(Column column) {
        try {
            final int t = column.getSQLType();
            return SqlDataType.decode(t);
        } catch (final Exception e) {
            LOGGER.error("Failed to retrieve SQL for type: {}", column.getType());
            LOGGER.error(e);
            return null;
        }
    }

    private static void analyzeTableTypes(Database db,
                                          RdbDatabase rdb) {
        rdb.tableType().name(TABLE).build();
        rdb.tableType().name(VIEW).build();
    }

    private void analyzeTables(Database db,
                               RdbDatabase rdb) throws IOException {
        final RdbCatalog rcatalog = rdb.catalog().build();
        final RdbSchema rschema = rcatalog.schema().build();
        for (final String tableName : db.getTableNames()) {
            final Table table = db.getTable(tableName);
            analyzeTable(table, tableName, rschema);
        }
    }

    private void analyzeSystemTables(Database db,
                                     RdbDatabase rdb) throws IOException {
        final RdbCatalog rcatalog =
                rdb.catalog()
                   .name("PUBLIC")// FIXME
                   .build();
        final RdbSchema rschema = rcatalog.schema().build();
        for (final String tableName : db.getSystemTableNames()) {
            final Table table = db.getSystemTable(tableName);
            analyzeTable(table, tableName, rschema);
        }
    }

    private void analyzeTable(Table table,
                              String tableName,
                              RdbSchema rschema) throws IOException {

        final PropertyMap props = table == null ? null : table.getProperties();
        final TableMetaData m = table == null ? null : table.getDatabase().getTableMetaData(tableName);
        final String tableTypeName;
        if (m == null) {
            tableTypeName = null;
        } else if (m.isLinked()) {
            tableTypeName = VIEW;
        } else {
            tableTypeName = TABLE;
        }

        final RdbTable rtable =
                rschema.table()
                       .name(tableName)
                       .comments(props == null ? null : getDescription(props))
                       .tableTypeName(tableTypeName)
                       .build();
        if (table != null) {
            if (!isEnabled(Hint.NO_COLUMNS)) {
                for (final Column column : table.getColumns()) {
                    analyzeColumn(column, rtable);
                }
            }

            if (!isEnabled(Hint.NO_PRIMARY_KEYS)) {
                analyzePrimaryKey(table, rtable);
            }
            if (!isEnabled(Hint.NO_FOREIGN_KEYS)) {
                analyzeForeignKeys(table, rtable);
            }
            if (!isEnabled(Hint.NO_INDICES)) {
                analyzeIndices(table, rtable);
            }
        }
    }

    private static void analyzeColumn(Column column,
                                      RdbTable rtable) throws IOException {
        final PropertyMap props = column.getProperties();
        rtable.column()
              .name(column.getName())
              .comments(getDescription(props))
              .ordinal(column.getColumnIndex() + 1)
              .size(column.getLength())
              .autoIncrement(YesNoUnknown.of(column.isAutoNumber()))
              .generated(YesNoUnknown.of(column.isCalculated()))
              .defaultValue(getDefaultValue(props))
              .dataType(getSqlDataType(column))
              .typeName(column.getType().name())
              .radix(column.getScale())
              .digits(column.getPrecision())
              .nullable(YesNoUnknown.of(isNullable(props)))
              .build();
        // TODO
    }

    private static void analyzePrimaryKey(Table table,
                                          RdbTable rtable) {
        try {
            final Index index = table.getPrimaryKeyIndex();
            final RdbPrimaryKey rpk =
                    rtable.primaryKey()
                          .name((table.getName() + "_" + index.getName()).toUpperCase())
                          .build();
            short ordinal = 0;
            for (final Index.Column pkc : index.getColumns()) {
                ordinal++;
                rpk.column()
                   .name(pkc.getName())
                   .ordinal(ordinal)
                   .build();
            }
        } catch (final IllegalArgumentException e) {
            // Ignore
        }
    }

    private static void analyzeForeignKeys(Table table,
                                           RdbTable rtable) throws IOException {
        for (final Index index : table.getIndexes()) {
            if (index.isForeignKey()) {
                final Index ref = index.getReferencedIndex();

                final RdbForeignKey rfk =
                        rtable.foreignKey()
                              .name((table.getName() + "_" + index.getName()).toUpperCase())
                              .refTableName(ref.getTable().getName())
                              .build();
                final List<? extends Index.Column> cols = index.getColumns();
                final List<? extends Index.Column> refcols = ref.getColumns();

                for (int i = 0; i < cols.size(); i++) {
                    final Index.Column col = cols.get(i);
                    final Index.Column refcol = refcols.get(i);
                    rfk.column()
                       .name(col.getName())
                       .ordinal((short) (i + 1))
                       .refColumnName(refcol.getName())
                       .build();
                }
            }
        }
    }

    private static void analyzeIndices(Table table,
                                       RdbTable rtable) {
        for (final Index index : table.getIndexes()) {
            if (!index.isForeignKey() && !index.isPrimaryKey()) {
                final RdbIndex ri =
                        rtable.index()
                              .name((table.getName() + "_" + index.getName()).toUpperCase())
                              .type(RdbIndexType.INDEX_OTHER)
                              .build();
                short ordinal = 0;
                for (final Index.Column ic : index.getColumns()) {
                    ordinal++;
                    ri.column()
                      .name(ic.getName())
                      .ordinal(ordinal)
                      .build();
                }
            }
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends RdbAbstractAnalyzer.Builder<Builder> {
        private File file;

        private Builder() {
        }

        public Builder file(File file) {
            this.file = file;
            return self();
        }

        public Builder filename(String filename) {
            this.file = new File(filename);
            return self();
        }

        @Override
        public RdbJackcessAnalyzer build() {
            return new RdbJackcessAnalyzer(this);
        }
    }
}