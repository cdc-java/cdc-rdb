package cdc.rdb.core;

import java.io.IOException;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import cdc.rdb.RdbDatabase;

public abstract class RdbAbstractAnalyzer {
    /** Hints */
    private final Set<Hint> hints;
    /** User name. */
    private final String user;
    /** User password. */
    private final String password;
    /** Set of schemas for which data must be collected. */
    private final Set<String> schemas = new HashSet<>();

    public enum Hint {
        NO_PROPERTIES,
        NO_DATA_TYPES,
        NO_USER_DATA_TYPES,
        NO_ATTRIBUTES,
        NO_TABLE_TYPES,
        NO_TABLES,
        NO_COLUMNS,
        NO_PRIMARY_KEYS,
        NO_FOREIGN_KEYS,
        NO_INDICES,
        NO_FUNCTIONS,
        NO_FUNCTION_COLUMNS,
        NO_PROCEDURES,
        NO_PROCEDURE_COLUMNS
    }

    protected RdbAbstractAnalyzer(Builder<?> builder) {
        this.hints = builder.hints;
        this.user = builder.user;
        this.password = builder.password;
        this.schemas.addAll(builder.schemas);
    }

    public final Set<Hint> getHints() {
        return hints;
    }

    public final boolean isEnabled(Hint hint) {
        return hints.contains(hint);
    }

    public final String getUser() {
        return user;
    }

    public final String getPassword() {
        return password;
    }

    protected final boolean acceptsSchema(String name) {
        return schemas.isEmpty() || schemas.contains(name);
    }

    protected final String getSchemaPattern() {
        if (schemas.size() == 1) {
            return schemas.iterator().next();
        } else {
            return null;
        }
    }

    public abstract RdbDatabase analyze() throws IOException;

    public abstract static class Builder<B extends Builder<B>> {
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);
        private String user;
        private String password;
        private final Set<String> schemas = new HashSet<>();

        protected Builder() {
        }

        @SuppressWarnings("unchecked")
        protected final B self() {
            return (B) this;
        }

        public final B hint(Hint hint) {
            this.hints.add(hint);
            return self();
        }

        public final B hints(Collection<Hint> hints) {
            this.hints.addAll(hints);
            return self();
        }

        public final B user(String user) {
            this.user = user;
            return self();
        }

        public final B password(String password) {
            this.password = password;
            return self();
        }

        public final B schema(String schema) {
            this.schemas.add(schema);
            return self();
        }

        public final B schemas(Collection<String> schemas) {
            this.schemas.addAll(schemas);
            return self();
        }

        public abstract RdbAbstractAnalyzer build();
    }
}