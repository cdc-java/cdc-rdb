package cdc.rdb.core;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.rdb.RdbDatabase;
import cdc.rdb.RdbDatabaseIo;

class RdbJackcessAnalyzerTest {
    private static final Logger LOGGER = LogManager.getLogger(RdbJackcessAnalyzerTest.class);

    @Test
    void test() throws IOException {
        final File input = new File("src/test/resources/Assets4_Data.mdb");
        LOGGER.info("Analyze {}", input);
        final RdbJackcessAnalyzer analyzer =
                RdbJackcessAnalyzer.builder()
                                   .file(input)
                                   .build();

        final RdbDatabase db = analyzer.analyze();
        LOGGER.info("Analyzed {}", input);

        final File output = new File("target/Assets4_Data-jackcess.xml");
        LOGGER.info("Save to {}", output);
        RdbDatabaseIo.print(db, output);
        LOGGER.info("Saved to {}", output);

        assertTrue(true);
    }
}