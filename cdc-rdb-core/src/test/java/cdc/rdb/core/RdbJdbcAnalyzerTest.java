package cdc.rdb.core;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.rdb.RdbDatabase;
import cdc.rdb.RdbDatabaseIo;

class RdbJdbcAnalyzerTest {
    private static final Logger LOGGER = LogManager.getLogger(RdbJdbcAnalyzerTest.class);

    @Test
    void test() throws IOException {
        final String url = "jdbc:ucanaccess://src/test/resources/Assets4_Data.mdb";
        LOGGER.info("Analyze {}", url);
        final RdbJdbcAnalyzer analyzer =
                RdbJdbcAnalyzer.builder()
                               .driver("net.ucanaccess.jdbc.UcanaccessDriver")
                               .url(url)
                               .build();

        final RdbDatabase db = analyzer.analyze();
        LOGGER.info("Analyzed {}", url);

        final File output = new File("target/Assets4_Data-jdbc.xml");
        LOGGER.info("Save to {}", output);
        RdbDatabaseIo.print(db, output);
        LOGGER.info("Saved to {}", output);

        assertTrue(true);
    }
}