package cdc.rdb.tools;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.rdb.RdbDatabase;
import cdc.rdb.RdbDatabaseIo;
import cdc.rdb.core.RdbAbstractAnalyzer;
import cdc.rdb.core.RdbJackcessAnalyzer;
import cdc.rdb.core.RdbJdbcAnalyzer;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;

/**
 * Class use to analyze database metadata and produce a corresponding structure.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbMetaAnalyzer {
    private static final Logger LOGGER = LogManager.getLogger(RdbMetaAnalyzer.class);

    private final RdbDatabase database;

    public static class MainArgs {
        public enum Feature implements OptionEnum {
            JACKCESS("jackcess",
                     "Use the Jackcess analyzer for an Access database instead of the provided JDBC driver."),
            NO_PROPERTIES("no-properties",
                          "Do not generate data related to properties.",
                          RdbAbstractAnalyzer.Hint.NO_PROPERTIES),
            NO_DATA_TYPES("no-data-types",
                          "Do not generate data related to data types.",
                          RdbAbstractAnalyzer.Hint.NO_DATA_TYPES),
            NO_USER_DATA_TYPES("no-user-data-types",
                               "Do not generate data related to user data types.",
                               RdbAbstractAnalyzer.Hint.NO_USER_DATA_TYPES),
            NO_ATTRIBUTES("no-attributes",
                          "Do not generate data related to user data types attributes.",
                          RdbAbstractAnalyzer.Hint.NO_ATTRIBUTES),
            NO_TABLE_TYPES("no-table-types",
                           "Do not generate data related to table types.",
                           RdbAbstractAnalyzer.Hint.NO_DATA_TYPES),
            NO_TABLES("no-tables",
                      "Do not generate data related to tables.",
                      RdbAbstractAnalyzer.Hint.NO_TABLES),
            NO_COLUMNS("no-columns",
                       "Do not generate data related to table columns.",
                       RdbAbstractAnalyzer.Hint.NO_COLUMNS),
            NO_PRIMARY_KEYS("no-primary-keys",
                            "Do not generate data related to primary keys.",
                            RdbAbstractAnalyzer.Hint.NO_PRIMARY_KEYS),
            NO_FOREIGN_KEYS("no-foreign-keys",
                            "Do not generate data related to foreign keys.",
                            RdbAbstractAnalyzer.Hint.NO_FOREIGN_KEYS),
            NO_INDICES("no-indices",
                       "Do not generate data related to indices.",
                       RdbAbstractAnalyzer.Hint.NO_INDICES),
            NO_FUNCTIONS("no-functions",
                         "Do not generate data related to functions.",
                         RdbAbstractAnalyzer.Hint.NO_FUNCTIONS),
            NO_FUNCTION_COLUMNS("no-function-columns",
                                "Do not generate data related to function columns.",
                                RdbAbstractAnalyzer.Hint.NO_FUNCTION_COLUMNS),
            NO_PROCEDURES("no-procedures",
                          "Do not generate data related to procedures.",
                          RdbAbstractAnalyzer.Hint.NO_PROCEDURES),
            NO_PROCEDURE_COLUMNS("no-procedure-columns",
                                 "Do not generate data related to procedure columns.",
                                 RdbAbstractAnalyzer.Hint.NO_PROCEDURE_COLUMNS);

            private final String name;
            private final String description;
            private final RdbAbstractAnalyzer.Hint hint;

            private Feature(String name,
                            String description,
                            RdbAbstractAnalyzer.Hint hint) {
                this.name = name;
                this.description = description;
                this.hint = hint;
            }

            private Feature(String name,
                            String description) {
                this(name, description, null);
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        /** Database url. */
        public String url;
        /** User name. */
        public String user;
        /** User password. */
        public String password;
        /** JDBC driver. */
        public String driver;
        /** Set of schemas for which data must be collected. */
        public final Set<String> schemas = new HashSet<>();
        /** XML output file. */
        public File xmlOutputFile;

        protected final FeatureMask<Feature> features = new FeatureMask<>();

        public final void setEnabled(Feature feature,
                                     boolean enabled) {
            features.setEnabled(feature, enabled);
        }

        public final boolean isEnabled(Feature feature) {
            return features.isEnabled(feature);
        }
    }

    private static Set<RdbAbstractAnalyzer.Hint> convert(FeatureMask<MainArgs.Feature> features) {
        // This works iff Hint and Feature are kept synchronized
        final Set<RdbAbstractAnalyzer.Hint> hints = EnumSet.noneOf(RdbAbstractAnalyzer.Hint.class);
        for (final MainArgs.Feature feature : MainArgs.Feature.values()) {
            if (features.contains(feature) && feature.hint != null) {
                hints.add(feature.hint);
            }
        }
        return hints;
    }

    private RdbMetaAnalyzer(MainArgs margs) throws IOException {
        LOGGER.info("Connect to: {} as: {}", margs.url, margs.user);

        final RdbAbstractAnalyzer analyzer;
        if (margs.isEnabled(MainArgs.Feature.JACKCESS)) {
            analyzer = RdbJackcessAnalyzer.builder()
                                          .filename(margs.url)
                                          .user(margs.user)
                                          .password(margs.password)
                                          .schemas(margs.schemas)
                                          .hints(convert(margs.features))
                                          .build();
        } else {
            analyzer = RdbJdbcAnalyzer.builder()
                                      .driver(margs.driver)
                                      .url(margs.url)
                                      .user(margs.user)
                                      .password(margs.password)
                                      .schemas(margs.schemas)
                                      .hints(convert(margs.features))
                                      .build();
        }

        this.database = analyzer.analyze();
    }

    public static RdbDatabase execute(MainArgs margs) throws IOException {
        final RdbMetaAnalyzer instance = new RdbMetaAnalyzer(margs);
        if (margs.xmlOutputFile != null) {
            LOGGER.info("Generate {}", margs.xmlOutputFile);
            RdbDatabaseIo.print(instance.database, margs.xmlOutputFile);
            LOGGER.info("Generated {}", margs.xmlOutputFile);
        }
        return instance.database;
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String SCHEMA = "schema";
        private static final String XML_OUTPUT = "xml";

        public MainSupport() {
            super(RdbMetaAnalyzer.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected String getHelpHeader() {
            return RdbMetaAnalyzer.class.getSimpleName()
                    + " analyzes meta data of a database, and generates an XML file with gathered information."
                    + "\nWith minimum options, everything is dumped. One must pass specific options to ignore some features.";
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(URL)
                                    .desc("""
                                            Mandatory URL of the database.
                                            Examples:
                                            - jdbc:ucanaccess://<path_to_db_file> (UCanAccess)
                                            - jdbc:derby://<path_to_db_directory> (Embedded Derby)
                                            - jdbc:derby://<host>[:<port>]/<db_name> (Server Derby)
                                            - <path_to_db_file> (--jackcess option)""")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(DRIVER)
                                    .desc("""
                                            Optional JDBC Driver class, which must be accessible in CLASSPATH.
                                            Examples:
                                            - net.ucanaccess.jdbc.UcanaccessDriver (UCanAccess).
                                            - org.apache.derby.jdbc.EmbeddedDriver (Embedded Derby).
                                            - org.apache.derby.jdbc.ClientDriver (Server Derby).""")

                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(USER)
                                    .desc("Optional user name.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder("pwd")
                                    .longOpt(PASSWORD)
                                    .desc("Optional user password.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(SCHEMA)
                                    .desc("Optional set of schemas that must be analyzed. When empty, all schemas are analyzed.")
                                    .hasArgs()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(XML_OUTPUT)
                                    .desc("Mandatory XML output filename.")
                                    .hasArg()
                                    .required()
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.url = cl.getOptionValue(URL);
            margs.user = cl.getOptionValue(USER);
            margs.password = cl.getOptionValue(PASSWORD);
            margs.driver = cl.getOptionValue(DRIVER);
            if (cl.hasOption(SCHEMA)) {
                for (final String s : cl.getOptionValues(SCHEMA)) {
                    margs.schemas.add(s);
                }
            }
            margs.xmlOutputFile = getValueAsFile(cl, XML_OUTPUT);

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            RdbMetaAnalyzer.execute(margs);
            return null;
        }
    }
}