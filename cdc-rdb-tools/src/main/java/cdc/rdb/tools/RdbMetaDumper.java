package cdc.rdb.tools;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.TableSection;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.time.Chronometer;

/**
 * Utility that reads meta data of a database and dumps it to an Office file.
 *
 * @author Damien Carbonne
 */
public final class RdbMetaDumper {
    private static final Logger LOGGER = LogManager.getLogger(RdbMetaDumper.class);
    private final MainArgs margs;

    private record Schema(String catalog,
                          String schema) {
    }

    private record Table(String catalog,
                         String schema,
                         String table) {
    }

    @FunctionalInterface
    private static interface SqlSupplier<R> {
        public R get() throws SQLException;
    }

    @FunctionalInterface
    private static interface SqlFunction<T, R> {
        public R apply(T t) throws SQLException;
    }

    @FunctionalInterface
    private static interface SqlBiFunction<T, U, R> {
        public R apply(T t,
                       U u) throws SQLException;
    }

    public static class MainArgs {
        /** Database url. */
        public String url;
        /** User name. */
        public String user;
        /** User password. */
        public String password;
        /** The file to generate. */
        public File outputFile;

        /** Set of schemas that must be dumped. If empty, all schemas are dumped. */
        public final Set<String> schemas = new HashSet<>();

        public final FeatureMask<Feature> features = new FeatureMask<>();

        public enum Feature implements OptionEnum {
            DUMP_ALL("dump-all", "Dump everything."),

            DUMP_FEATURES("dump-features", "Dump features."),
            DUMP_CATALOGS("dump-catalogs", "Dump catalogs."),
            DUMP_SCHEMAS("dump-schemas", "Dump schemas."),
            DUMP_CLIENT_INFO_PROPERTIES("dump-client-info-properties", "Dump client info properties."),
            DUMP_TYPE_INFOS("dump-type-infos", "Dump type infos."),
            DUMP_TABLE_TYPES("dump-table-types", "Dump table types."),
            DUMP_TABLES("dump-tables", "Dump tables."),
            DUMP_TABLE_PRIVILEGES("dump-table-privileges", "Dump table privileges."),
            DUMP_COLUMNS("dump-columns", "Dump columns."),
            DUMP_COLUMN_PRIVILEGES("dump-column-privileges", "Dump column privileges."),
            DUMP_SUPER_TABLES("dump-super-tables", "Dump super tables."),
            DUMP_SUPER_TYPES("dump-super-types", "Dump super types."),
            DUMP_USER_DATA_TYPES("dump-user-data-types", "Dump user data types."),
            DUMP_VERSION_COLUMNS("dump-version-columns", "Dump version columns."),
            DUMP_PSEUDO_COLUMNS("dump-pseudo-columns", "Dump pseudo columns."),
            DUMP_ATTRIBUTES("dump-attributes", "Dump attributes."),
            DUMP_INDEX_INFOS("dump-index-infos", "Dump index infos."),
            DUMP_FUNCTIONS("dump-functions", "Dump functions."),
            DUMP_FUNCTION_COLUMNS("dump-function-columns", "Dump function columns."),
            DUMP_PROCEDURES("dump-procedures", "Dump procedures."),
            DUMP_PROCEDURE_COLUMNS("dump-procedure-columns", "Dump procedure columns."),
            DUMP_PRIMARY_KEYS("dump-primary-keys", "Dump primary keys."),
            DUMP_EXPORTED_KEYS("dump-exported-keys", "Dump exported keys."),
            DUMP_IMPORTED_KEYS("dump-imported-keys", "Dump imported keys."),
            DUMP_CROSS_REFERENCES("dump-cross-references", "Dump cross references."),

            NO_DUMP_FEATURES("no-dump-features", "Do not dump features."),
            NO_DUMP_CATALOGS("no-dump-catalogs", "Do not dump catalogs."),
            NO_DUMP_SCHEMAS("no-dump-schemas", "Do not dump schemas."),
            NO_DUMP_CLIENT_INFO_PROPERTIES("no-dump-client-info-properties", "Do not dump client info properties."),
            NO_DUMP_TYPE_INFOS("no-dump-type-infos", "Do not dump type infos."),
            NO_DUMP_TABLE_TYPES("no-dump-table-types", "Do not dump table types."),
            NO_DUMP_TABLES("no-dump-tables", "Do not dump tables."),
            NO_DUMP_TABLE_PRIVILEGES("no-dump-table-privileges", "Do not dump table privileges."),
            NO_DUMP_COLUMNS("no-dump-columns", "Do not dump columns."),
            NO_DUMP_COLUMN_PRIVILEGES("no-dump-column-privileges", "Do not dump column privileges."),
            NO_DUMP_SUPER_TABLES("no-dump-super-tables", "Do not dump super tables."),
            NO_DUMP_SUPER_TYPES("no-dump-super-types", "Do not dump super types."),
            NO_DUMP_USER_DATA_TYPES("no-dump-user-data-types", "Do not dump user data types."),
            NO_DUMP_VERSION_COLUMNS("no-dump-version-columns", "Do not dump version columns."),
            NO_DUMP_PSEUDO_COLUMNS("no-dump-pseudo-columns", "Do not dump pseudo columns."),
            NO_DUMP_ATTRIBUTES("no-dump-attributes", "Do not dump attributes."),
            NO_DUMP_INDEX_INFOS("no-dump-index-infos", "Do not dump index infos."),
            NO_DUMP_FUNCTIONS("no-dump-functions", "Do not dump functions."),
            NO_DUMP_FUNCTION_COLUMNS("no-dump-function-columns", "Do not dump function columns."),
            NO_DUMP_PROCEDURES("no-dump-procedures", "Do not dump procedures."),
            NO_DUMP_PROCEDURE_COLUMNS("no-dump-procedure-columns", "Do not dump procedure columns."),
            NO_DUMP_PRIMARY_KEYS("no-dump-primary-keys", "Do not dump primary keys."),
            NO_DUMP_EXPORTED_KEYS("no-dump-exported-keys", "Do not dump exported keys."),
            NO_DUMP_IMPORTED_KEYS("no-dump-imported-keys", "Do not dump imported keys."),
            NO_DUMP_CROSS_REFERENCES("no-dump-cross-references", "Do not dump cross references."),

            VERBOSE("verbose", "Be verbose.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    private RdbMetaDumper(MainArgs margs) {
        this.margs = margs;
    }

    private void log(String message,
                     Object... params) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message, params);
        }
    }

    private void logSkip(Object target) {
        log("Skip {}", target);
    }

    private void logCreate(Object target) {
        log("Create {}", target);
    }

    private void logCreated(Object target,
                            Chronometer chrono) {
        log("Created {} ({})", target, chrono);
    }

    private boolean accepts(String schema) {
        return margs.schemas.isEmpty() || margs.schemas.contains(schema);
    }

    // private boolean accepts(Schema schema) {
    // return accepts(schema.schema);
    // }
    //
    // private boolean accepts(Table table) {
    // return accepts(table.schema);
    // }

    private void execute() throws SQLException, IOException {
        final Chronometer total = new Chronometer();
        total.start();
        final Chronometer chrono = new Chronometer();
        log("Connect to {} as {}", margs.url, margs.user);
        chrono.start();

        // Necessary to for UCanAccess/HSQLDB recent combination
        System.setProperty("hsqldb.method_class_names", "net.ucanaccess.converters.*");

        try (final Connection connection = DriverManager.getConnection(margs.url, margs.user, margs.password)) {
            chrono.suspend();
            log("Connected to {} ({})", margs.url, chrono);

            final DatabaseMetaData metadata = connection.getMetaData();

            // List all (catalog, schema) that match the filter criterion
            final List<Schema> schemas = new ArrayList<>();
            try (final ResultSet rs = metadata.getSchemas()) {
                while (rs.next()) {
                    final String name = rs.getString(1);
                    final String catalog = rs.getString(2);
                    if (accepts(name)) {
                        final Schema schema = new Schema(catalog, name);
                        schemas.add(schema);
                    }
                }
            }

            // List all (catalog, schema, table) that match the filter criterion
            final List<Table> tables = new ArrayList<>();
            try (final ResultSet rs = metadata.getTables(null, null, null, null)) {
                while (rs.next()) {
                    final String catalog = rs.getString(1);
                    final String schema = rs.getString(2);
                    final String name = rs.getString(3);
                    final Table table = new Table(catalog, schema, name);
                    tables.add(table);
                }
            }

            // Some queries must have a non null table name.
            // We use tables in that case.
            // DB seem to have different behavior when a null table is passed.
            // If possible we use schemas instead of tables to reduce the number of queries.

            chrono.start();
            final WorkbookWriterFactory factory = new WorkbookWriterFactory();
            logCreate(margs.outputFile);
            try (final WorkbookWriter<?> writer = factory.create(margs.outputFile, WorkbookWriterFeatures.STANDARD_BEST)) {
                addFeatures(writer, metadata);
                add(writer,
                    "Catalogs",
                    metadata::getCatalogs,
                    MainArgs.Feature.DUMP_CATALOGS,
                    MainArgs.Feature.NO_DUMP_CATALOGS);
                add(writer,
                    "Schemas",
                    metadata::getSchemas,
                    MainArgs.Feature.DUMP_SCHEMAS,
                    MainArgs.Feature.NO_DUMP_SCHEMAS);
                add(writer,
                    "Client Info Properties",
                    metadata::getClientInfoProperties,
                    MainArgs.Feature.DUMP_CLIENT_INFO_PROPERTIES,
                    MainArgs.Feature.NO_DUMP_CLIENT_INFO_PROPERTIES);
                add(writer,
                    "Type Infos",
                    metadata::getTypeInfo,
                    MainArgs.Feature.DUMP_TYPE_INFOS,
                    MainArgs.Feature.NO_DUMP_TYPE_INFOS);
                add(writer,
                    "Table Types",
                    metadata::getTableTypes,
                    MainArgs.Feature.DUMP_TABLE_TYPES,
                    MainArgs.Feature.NO_DUMP_TABLE_TYPES);

                add(writer,
                    "Tables",
                    schemas,
                    x -> metadata.getTables(x.catalog, x.schema, null, null),
                    MainArgs.Feature.DUMP_TABLES,
                    MainArgs.Feature.NO_DUMP_TABLES);
                add(writer,
                    "Table Privileges",
                    schemas,
                    x -> metadata.getTablePrivileges(x.catalog, x.schema, null),
                    MainArgs.Feature.DUMP_TABLE_PRIVILEGES,
                    MainArgs.Feature.NO_DUMP_TABLE_PRIVILEGES);
                add(writer,
                    "Super Tables",
                    schemas,
                    x -> metadata.getSuperTables(x.catalog, x.schema, null),
                    MainArgs.Feature.DUMP_SUPER_TABLES,
                    MainArgs.Feature.NO_DUMP_SUPER_TABLES);
                add(writer,
                    "Super Types",
                    schemas,
                    x -> metadata.getSuperTypes(x.catalog, x.schema, null),
                    MainArgs.Feature.DUMP_SUPER_TYPES,
                    MainArgs.Feature.NO_DUMP_SUPER_TYPES);
                add(writer,
                    "User Data Types",
                    schemas,
                    x -> metadata.getUDTs(x.catalog, x.schema, null, null),
                    MainArgs.Feature.DUMP_USER_DATA_TYPES,
                    MainArgs.Feature.NO_DUMP_USER_DATA_TYPES);
                add(writer,
                    "Version Columns",
                    tables,
                    x -> metadata.getVersionColumns(x.catalog, x.schema, x.table),
                    MainArgs.Feature.DUMP_VERSION_COLUMNS,
                    MainArgs.Feature.NO_DUMP_VERSION_COLUMNS);

                // TODO add(writer, "Best Row Identifiers", () -> metadata.getBestRowIdentifier(null, null, null, ?, true));

                add(writer,
                    "Columns",
                    schemas,
                    x -> metadata.getColumns(x.catalog, x.schema, null, null),
                    MainArgs.Feature.DUMP_COLUMNS,
                    MainArgs.Feature.NO_DUMP_COLUMNS);
                add(writer,
                    "Column Privileges",
                    tables,
                    x -> metadata.getColumnPrivileges(x.catalog, x.schema, x.table, null),
                    MainArgs.Feature.DUMP_COLUMN_PRIVILEGES,
                    MainArgs.Feature.NO_DUMP_COLUMN_PRIVILEGES);
                add(writer,
                    "Pseudo Columns",
                    tables,
                    x -> metadata.getPseudoColumns(x.catalog, x.schema, x.table, null),
                    MainArgs.Feature.DUMP_PSEUDO_COLUMNS,
                    MainArgs.Feature.NO_DUMP_PSEUDO_COLUMNS);
                add(writer,
                    "Attributes",
                    schemas,
                    x -> metadata.getAttributes(x.catalog, x.schema, null, null),
                    MainArgs.Feature.DUMP_ATTRIBUTES,
                    MainArgs.Feature.NO_DUMP_ATTRIBUTES);
                add(writer,
                    "Index Infos",
                    tables,
                    x -> metadata.getIndexInfo(x.catalog, x.schema, x.table, false, false),
                    MainArgs.Feature.DUMP_INDEX_INFOS,
                    MainArgs.Feature.NO_DUMP_INDEX_INFOS);
                add(writer,
                    "Functions",
                    schemas,
                    x -> metadata.getFunctions(x.catalog, x.schema, null),
                    MainArgs.Feature.DUMP_FUNCTIONS,
                    MainArgs.Feature.NO_DUMP_FUNCTIONS);
                add(writer,
                    "Function Columns",
                    schemas,
                    x -> metadata.getFunctionColumns(x.catalog, x.schema, null, null),
                    MainArgs.Feature.DUMP_FUNCTION_COLUMNS,
                    MainArgs.Feature.NO_DUMP_FUNCTION_COLUMNS);
                add(writer,
                    "Procedures",
                    schemas,
                    x -> metadata.getProcedures(x.catalog, x.schema, null),
                    MainArgs.Feature.DUMP_PROCEDURES,
                    MainArgs.Feature.NO_DUMP_PROCEDURES);
                add(writer,
                    "Procedure Columns",
                    schemas,
                    x -> metadata.getProcedureColumns(x.catalog, x.schema, null, null),
                    MainArgs.Feature.DUMP_PROCEDURE_COLUMNS,
                    MainArgs.Feature.NO_DUMP_PROCEDURE_COLUMNS);
                add(writer,
                    "Primary Keys",
                    tables,
                    x -> metadata.getPrimaryKeys(x.catalog, x.schema, x.table),
                    MainArgs.Feature.DUMP_PRIMARY_KEYS,
                    MainArgs.Feature.NO_DUMP_PRIMARY_KEYS);
                add(writer,
                    "Exported Keys",
                    tables,
                    x -> metadata.getExportedKeys(x.catalog, x.schema, x.table),
                    MainArgs.Feature.DUMP_EXPORTED_KEYS,
                    MainArgs.Feature.NO_DUMP_EXPORTED_KEYS);
                add(writer,
                    "Imported Keys",
                    tables,
                    x -> metadata.getImportedKeys(x.catalog, x.schema, x.table),
                    MainArgs.Feature.DUMP_IMPORTED_KEYS,
                    MainArgs.Feature.NO_DUMP_IMPORTED_KEYS);
                add(writer,
                    "Cross References",
                    tables,
                    (x,
                     y) -> metadata.getCrossReference(x.catalog, x.schema, x.table, y.catalog, y.schema, y.table),
                    MainArgs.Feature.DUMP_CROSS_REFERENCES,
                    MainArgs.Feature.NO_DUMP_CROSS_REFERENCES);
            }
            chrono.suspend();
            logCreated(margs.outputFile, chrono);
        }
        total.suspend();
        log("Done (" + total + ")");
    }

    private void add(WorkbookWriter<?> writer,
                     String sheet,
                     SqlSupplier<ResultSet> supplier,
                     MainArgs.Feature with,
                     MainArgs.Feature without) throws IOException {
        if ((margs.features.contains(MainArgs.Feature.DUMP_ALL) ||
                margs.features.contains(with)) &&
                !margs.features.contains(without)) {
            final Chronometer chrono = new Chronometer();
            chrono.start();

            logCreate(sheet);
            writer.beginSheet(sheet);
            try (final ResultSet rs = supplier.get()) {
                final ResultSetMetaData meta = rs.getMetaData();
                addHeader(writer, meta);
                addData(writer, meta, rs);
            } catch (final SQLException e) {
                LOGGER.error("Failed to analyse {} {}", sheet, e.getClass().getSimpleName());
                writer.beginRow(TableSection.DATA);
                writer.addCell(e.getClass().getSimpleName() + "\n" + e.getMessage());
            }
            chrono.suspend();
            logCreated(sheet, chrono);
        } else {
            logSkip(sheet);
        }
    }

    private <T> void add(WorkbookWriter<?> writer,
                         String sheet,
                         List<T> items,
                         SqlFunction<T, ResultSet> supplier,
                         MainArgs.Feature with,
                         MainArgs.Feature without) throws IOException {
        if ((margs.features.contains(MainArgs.Feature.DUMP_ALL) ||
                margs.features.contains(with)) &&
                !margs.features.contains(without)) {
            final Chronometer chrono = new Chronometer();
            chrono.start();

            logCreate(sheet);
            writer.beginSheet(sheet);
            boolean first = true;
            int index = 0;
            for (final T item : items) {
                index++;
                log("   " + index + "/" + items.size() + " " + item);
                try (final ResultSet rs = supplier.apply(item)) {
                    final ResultSetMetaData meta = rs.getMetaData();
                    if (first) {
                        addHeader(writer, meta);
                        first = false;
                    }
                    addData(writer, meta, rs);
                } catch (final SQLException e) {
                    LOGGER.error("Failed to analyse {} {} {}", sheet, item, e.getClass().getSimpleName());
                    writer.beginRow(TableSection.DATA);
                    writer.addCell(item + "\n" + e.getClass().getSimpleName() + "\n" + e.getMessage());
                }
            }
            chrono.suspend();
            logCreated(sheet, chrono);
        } else {
            logSkip(sheet);
        }
    }

    private <T> void add(WorkbookWriter<?> writer,
                         String sheet,
                         List<T> items,
                         SqlBiFunction<T, T, ResultSet> supplier,
                         MainArgs.Feature with,
                         MainArgs.Feature without) throws IOException {
        if ((margs.features.contains(MainArgs.Feature.DUMP_ALL) ||
                margs.features.contains(with)) &&
                !margs.features.contains(without)) {
            final Chronometer chrono = new Chronometer();
            chrono.start();

            logCreate(sheet);
            writer.beginSheet(sheet);
            boolean first = true;
            int index = 0;
            final int size = items.size() * items.size();
            for (final T item1 : items) {
                for (final T item2 : items) {
                    index++;
                    log("   " + index + "/" + size + " " + item1 + " " + item2);
                    try (final ResultSet rs = supplier.apply(item1, item2)) {
                        final ResultSetMetaData meta = rs.getMetaData();
                        if (first) {
                            addHeader(writer, meta);
                            first = false;
                        }
                        addData(writer, meta, rs);
                    } catch (final SQLException e) {
                        LOGGER.error("Failed to analyse {} {} {} {}", sheet, item1, item2, e.getClass().getSimpleName());
                        writer.beginRow(TableSection.DATA);
                        writer.addCell(item1 + "\n" + item2 + "\n" + e.getClass().getSimpleName() + "\n" + e.getMessage());
                    }
                }
            }
            chrono.suspend();
            logCreated(sheet, chrono);
        } else {
            logSkip(sheet);
        }
    }

    private static void addHeader(WorkbookWriter<?> writer,
                                  ResultSetMetaData meta) throws IOException, SQLException {
        writer.beginRow(TableSection.HEADER);
        for (int index = 1; index <= meta.getColumnCount(); index++) {
            writer.addCell(meta.getColumnName(index));
        }
        writer.beginRow(TableSection.DATA);
        for (int index = 1; index <= meta.getColumnCount(); index++) {
            writer.addCell(meta.getColumnTypeName(index));
        }
        writer.beginRow(TableSection.DATA);
        for (int index = 1; index <= meta.getColumnCount(); index++) {
            writer.addCell(meta.getColumnClassName(index));
        }
    }

    private static void addData(WorkbookWriter<?> writer,
                                ResultSetMetaData meta,
                                ResultSet rs) throws SQLException, IOException {
        while (rs.next()) {
            writer.beginRow(TableSection.DATA);
            for (int index = 1; index <= meta.getColumnCount(); index++) {
                writer.addCell(rs.getObject(index));
            }
        }
    }

    private void addFeatures(WorkbookWriter<?> writer,
                             DatabaseMetaData metadata) throws IOException {
        final String sheet = "Features";
        if ((margs.features.contains(MainArgs.Feature.DUMP_ALL) ||
                margs.features.contains(MainArgs.Feature.DUMP_FEATURES)) &&
                !margs.features.contains(MainArgs.Feature.NO_DUMP_FEATURES)) {
            final Chronometer chrono = new Chronometer();
            chrono.start();

            logCreate(sheet);
            writer.beginSheet(sheet);
            writer.beginRow(TableSection.HEADER);
            writer.addCells("Name", "Value");

            addFeature(writer, "All Procedures Are Callable", metadata::allProceduresAreCallable);
            addFeature(writer, "All Tables Are Selectable", metadata::allTablesAreSelectable);
            addFeature(writer, "Auto Commit Failure Closes All ResultSets", metadata::autoCommitFailureClosesAllResultSets);
            addFeature(writer, "Data Definition Causes Transaction Commit", metadata::dataDefinitionCausesTransactionCommit);
            addFeature(writer, "Data Definition Ignored In Transactions", metadata::dataDefinitionIgnoredInTransactions);
            addFeature(writer, "Does Max Row Size Include Blobs", metadata::doesMaxRowSizeIncludeBlobs);
            addFeature(writer, "Generated Key Always Returned", metadata::generatedKeyAlwaysReturned);
            addFeature(writer, "Is Catalog At Start", metadata::isCatalogAtStart);
            addFeature(writer, "Is Read Only", metadata::isReadOnly);
            addFeature(writer, "Locators Update Copy", metadata::locatorsUpdateCopy);
            addFeature(writer, "Null Plus Non Null Is Null", metadata::nullPlusNonNullIsNull);
            addFeature(writer, "Nulls Are Sorted At End", metadata::nullsAreSortedAtEnd);
            addFeature(writer, "Nulls Are Sorted At Start", metadata::nullsAreSortedAtStart);
            addFeature(writer, "Nulls Are Sorted High", metadata::nullsAreSortedHigh);
            addFeature(writer, "Nulls Are Sorted Low", metadata::nullsAreSortedLow);
            addFeature(writer, "Stores Lower Case Identifiers", metadata::storesLowerCaseIdentifiers);
            addFeature(writer, "Stores Lower Case Quoted Identifiers", metadata::storesLowerCaseQuotedIdentifiers);
            addFeature(writer, "Stores Mixed Case Identifiers", metadata::storesMixedCaseIdentifiers);
            addFeature(writer, "Stores Mixed Case Quoted Identifiers", metadata::storesMixedCaseQuotedIdentifiers);
            addFeature(writer, "Stores Upper Case Identifiers", metadata::storesUpperCaseIdentifiers);
            addFeature(writer, "Stores Upper Case Quoted Identifiers", metadata::storesUpperCaseQuotedIdentifiers);
            addFeature(writer, "Uses Local File Per Table", metadata::usesLocalFilePerTable);
            addFeature(writer, "Uses Local Files", metadata::usesLocalFiles);

            addFeature(writer, "URL", metadata::getURL);
            addFeature(writer, "User Name", metadata::getUserName);

            addFeature(writer, "JDBC Major Version", metadata::getJDBCMajorVersion);
            addFeature(writer, "JDBC Minor Version", metadata::getJDBCMinorVersion);

            addFeature(writer, "Driver Major Version", metadata::getDriverMajorVersion);
            addFeature(writer, "Driver Minor Version", metadata::getDriverMinorVersion);
            addFeature(writer, "Driver Name", metadata::getDriverName);
            addFeature(writer, "Driver Version", metadata::getDriverVersion);

            addFeature(writer, "Database Product Name", metadata::getDatabaseProductName);
            addFeature(writer, "Database Product Version", metadata::getDatabaseProductVersion);
            addFeature(writer, "Database Major Version", metadata::getDatabaseMajorVersion);
            addFeature(writer, "Database Minor Version", metadata::getDatabaseMinorVersion);

            addFeature(writer, "Catalog Separator", metadata::getCatalogSeparator);

            addFeature(writer, "Catalog Term", metadata::getCatalogTerm);
            addFeature(writer, "Schema Term", metadata::getSchemaTerm);
            addFeature(writer, "Procedure Term", metadata::getProcedureTerm);

            addFeature(writer, "Default Transaction Isolation", metadata::getDefaultTransactionIsolation);

            addFeature(writer, "Extra Name Characters", metadata::getExtraNameCharacters);
            addFeature(writer, "Identifier Quote String", metadata::getIdentifierQuoteString);

            addFeature(writer, "Max Binary Literal Length", metadata::getMaxBinaryLiteralLength);
            addFeature(writer, "Max Catalog Name Length", metadata::getMaxCatalogNameLength);
            addFeature(writer, "Max Char Literal Length", metadata::getMaxCharLiteralLength);
            addFeature(writer, "Max Column Name Length", metadata::getMaxColumnNameLength);
            addFeature(writer, "Max Columns In Group By", metadata::getMaxColumnsInGroupBy);
            addFeature(writer, "Max Columns In Index", metadata::getMaxColumnsInIndex);
            addFeature(writer, "Max Columns In Order By", metadata::getMaxColumnsInOrderBy);
            addFeature(writer, "Max Columns In Select", metadata::getMaxColumnsInSelect);
            addFeature(writer, "Max Columns In Table", metadata::getMaxColumnsInTable);
            addFeature(writer, "Max Connections", metadata::getMaxConnections);
            addFeature(writer, "Max Cursor Name Length", metadata::getMaxCursorNameLength);
            addFeature(writer, "Max Index Length", metadata::getMaxIndexLength);
            addFeature(writer, "Max Logical Lob Size", metadata::getMaxLogicalLobSize);
            addFeature(writer, "Max Procedure Name Length", metadata::getMaxProcedureNameLength);
            addFeature(writer, "Max Row Size", metadata::getMaxRowSize);
            addFeature(writer, "Max Schema Name Length", metadata::getMaxSchemaNameLength);
            addFeature(writer, "Max Statement Length", metadata::getMaxStatementLength);
            addFeature(writer, "Max Statments", metadata::getMaxStatements);
            addFeature(writer, "Max Table Name Length", metadata::getMaxTableNameLength);
            addFeature(writer, "Max Tables In Select", metadata::getMaxTablesInSelect);
            addFeature(writer, "Max User Name Length", metadata::getMaxUserNameLength);

            addFeature(writer, "Numeric Functions", metadata::getNumericFunctions);
            addFeature(writer, "String Functions", metadata::getStringFunctions);
            addFeature(writer, "System Functions", metadata::getSystemFunctions);
            addFeature(writer, "Time Date Functions", metadata::getTimeDateFunctions);

            addFeature(writer, "ResultSet Holdability", metadata::getResultSetHoldability);
            addFeature(writer, "RowId Lifetime", metadata::getRowIdLifetime);
            addFeature(writer, "Search String Escape", metadata::getSearchStringEscape);

            addFeature(writer, "SQL Keywords", metadata::getSQLKeywords);
            addFeature(writer, "SQL State Type", metadata::getSQLStateType);

            // Supports

            addFeature(writer, "Supports Alter Table With Add Column", metadata::supportsAlterTableWithAddColumn);
            addFeature(writer, "Supports Alter Table With Drop Column", metadata::supportsAlterTableWithDropColumn);

            addFeature(writer, "Supports ANSI92 Entry Level SQL", metadata::supportsANSI92EntryLevelSQL);
            addFeature(writer, "Supports ANSI92 Full SQL", metadata::supportsANSI92FullSQL);
            addFeature(writer, "Supports ANSI92 Intermediate SQL", metadata::supportsANSI92IntermediateSQL);

            addFeature(writer, "Supports Batch Updates", metadata::supportsBatchUpdates);

            addFeature(writer, "Supports Catalogs In Data Manipulation", metadata::supportsCatalogsInDataManipulation);
            addFeature(writer, "Supports Catalogs In Index Definitions", metadata::supportsCatalogsInIndexDefinitions);
            addFeature(writer, "Supports Catalogs In Privilege Definitions", metadata::supportsCatalogsInPrivilegeDefinitions);
            addFeature(writer, "Supports Catalogs In Procedure Calls", metadata::supportsCatalogsInProcedureCalls);
            addFeature(writer, "Supports Catalogs In Table Definitions", metadata::supportsCatalogsInTableDefinitions);

            addFeature(writer, "Supports Column Aliasing", metadata::supportsColumnAliasing);

            addFeature(writer, "Supports Convert", metadata::supportsConvert);

            addFeature(writer, "Supports Core SQL Grammar", metadata::supportsCoreSQLGrammar);
            addFeature(writer, "Supports Minimum SQL Grammar", metadata::supportsMinimumSQLGrammar);
            addFeature(writer, "Supports Extended SQL Grammar", metadata::supportsExtendedSQLGrammar);

            addFeature(writer, "Supports Correlated Subqueries", metadata::supportsCorrelatedSubqueries);
            addFeature(writer,
                       "Supports Data Definition And Data Manipulation Transactions",
                       metadata::supportsDataDefinitionAndDataManipulationTransactions);
            addFeature(writer, "Supports Data Manipulation Transactions Only", metadata::supportsDataManipulationTransactionsOnly);
            addFeature(writer, "Supports Different Table Correlation Names", metadata::supportsDifferentTableCorrelationNames);
            addFeature(writer, "Supports Expressions In Order By", metadata::supportsExpressionsInOrderBy);
            addFeature(writer, "Supports Full Outer Joins", metadata::supportsFullOuterJoins);
            addFeature(writer, "Supports Get Generated Keys", metadata::supportsGetGeneratedKeys);

            addFeature(writer, "Supports Group By", metadata::supportsGroupBy);
            addFeature(writer, "Supports Group By Beyond Select", metadata::supportsGroupByBeyondSelect);
            addFeature(writer, "Supports Group By Unrelated", metadata::supportsGroupByUnrelated);

            addFeature(writer, "Supports Integrity Enhancement Facility", metadata::supportsIntegrityEnhancementFacility);
            addFeature(writer, "Supports Like Escape Clause", metadata::supportsLikeEscapeClause);

            addFeature(writer, "Supports Mixed Case Identifiers", metadata::supportsMixedCaseIdentifiers);
            addFeature(writer, "Supports Mixed Case Quoted Identifiers", metadata::supportsMixedCaseQuotedIdentifiers);

            addFeature(writer, "Supports Multiple Open Results", metadata::supportsMultipleOpenResults);
            addFeature(writer, "Supports Multiple ResultSets", metadata::supportsMultipleResultSets);
            addFeature(writer, "Supports Multiple Transactions", metadata::supportsMultipleTransactions);
            addFeature(writer, "Supports Named Parameters", metadata::supportsNamedParameters);
            addFeature(writer, "Supports Non Nullable Columns", metadata::supportsNonNullableColumns);
            addFeature(writer, "Supports Open Cursors Across Commit", metadata::supportsOpenCursorsAcrossCommit);
            addFeature(writer, "Supports Open Cursors Across Rollback", metadata::supportsOpenCursorsAcrossRollback);
            addFeature(writer, "Supports Open Statements Across Commit", metadata::supportsOpenStatementsAcrossCommit);
            addFeature(writer, "Supports Open Statements Across Rollback", metadata::supportsOpenStatementsAcrossRollback);
            addFeature(writer, "Supports Order By Unrelated", metadata::supportsOrderByUnrelated);

            addFeature(writer, "Supports Limited Outer Joins", metadata::supportsLimitedOuterJoins);
            addFeature(writer, "Supports Outer Joins", metadata::supportsOuterJoins);

            addFeature(writer, "Supports Positioned Delete", metadata::supportsPositionedDelete);
            addFeature(writer, "Supports Positioned Update", metadata::supportsPositionedUpdate);

            addFeature(writer, "Supports Ref Cursors", metadata::supportsRefCursors);
            addFeature(writer, "Supports Savepoints", metadata::supportsSavepoints);

            addFeature(writer, "Supports Schemas In Data Manipulation", metadata::supportsSchemasInDataManipulation);
            addFeature(writer, "Supports Schemas In Index Definitions", metadata::supportsSchemasInIndexDefinitions);
            addFeature(writer, "Supports Schemas In Privilege Definitions", metadata::supportsSchemasInPrivilegeDefinitions);
            addFeature(writer, "Supports Schemas In Procedure Calls", metadata::supportsSchemasInProcedureCalls);
            addFeature(writer, "Supports Schemas In Table Definitions", metadata::supportsSchemasInTableDefinitions);

            addFeature(writer, "Supports Select For Update", metadata::supportsSelectForUpdate);
            addFeature(writer, "Supports Statement Pooling", metadata::supportsStatementPooling);
            addFeature(writer, "Supports Stored Functions Using Call Syntax", metadata::supportsStoredFunctionsUsingCallSyntax);
            addFeature(writer, "Supports Stored Procedures", metadata::supportsStoredProcedures);

            addFeature(writer, "Supports Subqueries In Comparisons", metadata::supportsSubqueriesInComparisons);
            addFeature(writer, "Supports Subqueries In Exists", metadata::supportsSubqueriesInExists);
            addFeature(writer, "Supports Subqueries In Ins", metadata::supportsSubqueriesInIns);
            addFeature(writer, "Supports Subqueries In Quantifieds", metadata::supportsSubqueriesInQuantifieds);

            addFeature(writer, "Supports Table Correlation Names", metadata::supportsTableCorrelationNames);
            addFeature(writer, "Supports Transactions", metadata::supportsTransactions);
            addFeature(writer, "Supports Union", metadata::supportsUnion);
            addFeature(writer, "Supports Union All", metadata::supportsUnionAll);
            chrono.suspend();
            logCreated(sheet, chrono);
        } else {
            logSkip(sheet);
        }
    }

    private static void addFeature(WorkbookWriter<?> writer,
                                   String name,
                                   SqlSupplier<?> supplier) throws IOException {
        writer.beginRow(TableSection.DATA);
        writer.addCell(name);
        try {
            final Object value = supplier.get();
            writer.addCell(value);
        } catch (final RuntimeException | SQLException e) {
            writer.addCell("Failed: " + e.getClass().getSimpleName() + " " + e.getMessage());
        }
    }

    public static void execute(MainArgs margs) throws SQLException, IOException {
        final RdbMetaDumper meta = new RdbMetaDumper(margs);
        meta.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String SCHEMA = "schema";

        public MainSupport() {
            super(RdbMetaDumper.class,
                  LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected String getHelpHeader() {
            return RdbMetaDumper.class.getSimpleName()
                    + " dumps meta data of a database to an Office file (CSV, XLS, XLSX, ...).";
        }

        @Override
        protected String getHelpFooter() {
            return """
                    LIMITATIONS
                    The implementation of --dump-cross-references is inefficient (O(n^2)).""";
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(URL)
                                    .desc("""
                                            Mandatory URL of the database.
                                            Examples:
                                            - jdbc:ucanaccess://<path_to_db_file>;showSchema=true (UCanAccess)
                                            - jdbc:derby://<path_to_db_directory> (Embedded Derby)
                                            - jdbc:derby://<host>[:<port>]/<db_name> (Server Derby)""")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(USER)
                                    .desc("Optional user name.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder("pwd")
                                    .longOpt(PASSWORD)
                                    .desc("Optional user password.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(OUTPUT)
                                    .desc("Output file. If must have a recognized Office extension (case does not matter).")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(SCHEMA)
                                    .desc("Optional name(s) of schema(s) to dump.")
                                    .hasArgs()
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);

            createGroup(options, MainArgs.Feature.DUMP_ATTRIBUTES, MainArgs.Feature.NO_DUMP_ATTRIBUTES);
            createGroup(options, MainArgs.Feature.DUMP_CATALOGS, MainArgs.Feature.NO_DUMP_CATALOGS);
            createGroup(options, MainArgs.Feature.DUMP_CLIENT_INFO_PROPERTIES, MainArgs.Feature.NO_DUMP_CLIENT_INFO_PROPERTIES);
            createGroup(options, MainArgs.Feature.DUMP_COLUMN_PRIVILEGES, MainArgs.Feature.NO_DUMP_COLUMN_PRIVILEGES);
            createGroup(options, MainArgs.Feature.DUMP_COLUMNS, MainArgs.Feature.NO_DUMP_COLUMNS);
            createGroup(options, MainArgs.Feature.DUMP_CROSS_REFERENCES, MainArgs.Feature.NO_DUMP_CROSS_REFERENCES);
            createGroup(options, MainArgs.Feature.DUMP_EXPORTED_KEYS, MainArgs.Feature.NO_DUMP_EXPORTED_KEYS);
            createGroup(options, MainArgs.Feature.DUMP_FEATURES, MainArgs.Feature.NO_DUMP_FEATURES);
            createGroup(options, MainArgs.Feature.DUMP_FUNCTION_COLUMNS, MainArgs.Feature.NO_DUMP_FUNCTION_COLUMNS);
            createGroup(options, MainArgs.Feature.DUMP_FUNCTIONS, MainArgs.Feature.NO_DUMP_FUNCTIONS);
            createGroup(options, MainArgs.Feature.DUMP_IMPORTED_KEYS, MainArgs.Feature.NO_DUMP_IMPORTED_KEYS);
            createGroup(options, MainArgs.Feature.DUMP_INDEX_INFOS, MainArgs.Feature.NO_DUMP_INDEX_INFOS);
            createGroup(options, MainArgs.Feature.DUMP_PRIMARY_KEYS, MainArgs.Feature.NO_DUMP_PRIMARY_KEYS);
            createGroup(options, MainArgs.Feature.DUMP_PROCEDURE_COLUMNS, MainArgs.Feature.NO_DUMP_PROCEDURE_COLUMNS);
            createGroup(options, MainArgs.Feature.DUMP_PROCEDURES, MainArgs.Feature.NO_DUMP_PROCEDURES);
            createGroup(options, MainArgs.Feature.DUMP_PSEUDO_COLUMNS, MainArgs.Feature.NO_DUMP_PSEUDO_COLUMNS);
            createGroup(options, MainArgs.Feature.DUMP_SCHEMAS, MainArgs.Feature.NO_DUMP_SCHEMAS);
            createGroup(options, MainArgs.Feature.DUMP_SUPER_TABLES, MainArgs.Feature.NO_DUMP_SUPER_TABLES);
            createGroup(options, MainArgs.Feature.DUMP_SUPER_TYPES, MainArgs.Feature.NO_DUMP_SUPER_TYPES);
            createGroup(options, MainArgs.Feature.DUMP_TABLE_PRIVILEGES, MainArgs.Feature.NO_DUMP_TABLE_PRIVILEGES);
            createGroup(options, MainArgs.Feature.DUMP_TABLE_TYPES, MainArgs.Feature.NO_DUMP_TABLE_TYPES);
            createGroup(options, MainArgs.Feature.DUMP_TABLES, MainArgs.Feature.NO_DUMP_TABLES);
            createGroup(options, MainArgs.Feature.DUMP_TYPE_INFOS, MainArgs.Feature.NO_DUMP_TYPE_INFOS);
            createGroup(options, MainArgs.Feature.DUMP_USER_DATA_TYPES, MainArgs.Feature.NO_DUMP_USER_DATA_TYPES);
            createGroup(options, MainArgs.Feature.DUMP_VERSION_COLUMNS, MainArgs.Feature.NO_DUMP_VERSION_COLUMNS);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.url = cl.getOptionValue(URL);
            margs.user = cl.getOptionValue(USER);
            margs.password = cl.getOptionValue(PASSWORD);
            margs.outputFile = getValueAsFile(cl, OUTPUT);
            fillValues(cl, SCHEMA, margs.schemas);
            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);
            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            RdbMetaDumper.execute(margs);
            return null;
        }
    }
}