package cdc.rdb.tools.dump.config;

import java.util.Collection;

import cdc.util.lang.Checks;

/**
 * Base abstract configuration node.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractConfig {
    /**
     * Parent config.
     * <p>
     * Used to inherit default processing.
     */
    private final AbstractConfig parent;

    /**
     * Default processing.
     * <p>
     * Can be overridden, whatever the parent says.
     */
    private Processing processing = Processing.INHERIT;

    private String note;

    AbstractConfig(AbstractConfig parent) {
        this.parent = parent;
    }

    protected AbstractConfig(Builder<?, ?> builder) {
        this.parent = builder.parent;
        this.processing = Checks.isNotNull(builder.processing, "processing");
        this.note = builder.note;
        if (parent == null && processing == Processing.INHERIT) {
            throw new IllegalArgumentException("Can not set " + processing + " on root config.");
        }
    }

    public AbstractConfig getParent() {
        return parent;
    }

    /**
     * Sets the processing of this configuration node.
     *
     * @param processing The processing.
     * @throws IllegalArgumentException When {@code processing} is {@code null},
     *             or is {@link Processing#INHERIT} on a root node.
     */
    public final void setProcessing(Processing processing) {
        Checks.isNotNull(processing, "processing");
        if (parent == null && processing == Processing.INHERIT) {
            throw new IllegalArgumentException("Can not set " + processing + " on root config.");
        }

        this.processing = processing;
    }

    /**
     * @return The processing of this configuration node.
     */
    public final Processing getProcessing() {
        return processing;
    }

    /**
     * @return The inherited processing of this configuration node.<br>
     *         If this node is a root node, then it is the processing of this node
     *         (which can not be {@link Processing#INHERIT}).<br>
     *         Otherwise, if the processing of this node is {@link Processing#INHERIT}, it is the inherited
     *         processing of its parent node.<br>
     *         Otherwise, it is the processing of this node.
     */
    public final EffectiveProcessing getInheritedProcessing() {
        if (parent == null) {
            // Should be KEEP or IGNORE
            return EffectiveProcessing.from(processing);
        } else {
            if (processing == Processing.INHERIT) {
                return parent.getInheritedProcessing();
            } else {
                return EffectiveProcessing.from(processing);
            }
        }
    }

    /**
     * @return The effective processing of this node.
     */
    public abstract EffectiveProcessing getEffectiveProcessing();

    protected EffectiveProcessing toEffectiveProcessing(Collection<? extends AbstractConfig> children) {
        if (children.isEmpty()) {
            return getInheritedProcessing();
        } else {
            for (final AbstractConfig child : children) {
                if (child.getEffectiveProcessing() == EffectiveProcessing.KEEP) {
                    return EffectiveProcessing.KEEP;
                }
            }
            return EffectiveProcessing.IGNORE;
        }
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public abstract static class Builder<C extends AbstractConfig, B extends Builder<C, B>> {
        protected final AbstractConfig parent;
        private Processing processing = Processing.KEEP;
        private String note = null;

        protected Builder(AbstractConfig parent) {
            this.parent = parent;
        }

        protected abstract B self();

        public B processing(Processing processing) {
            this.processing = processing;
            return self();
        }

        public B note(String note) {
            this.note = note;
            return self();
        }

        public abstract C build();
    }
}