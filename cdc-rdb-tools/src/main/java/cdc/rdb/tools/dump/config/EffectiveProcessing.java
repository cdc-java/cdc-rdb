package cdc.rdb.tools.dump.config;

import cdc.util.lang.UnexpectedValueException;

public enum EffectiveProcessing {
    KEEP,
    IGNORE;

    public static EffectiveProcessing from(Processing processing) {
        switch (processing) {
        case IGNORE:
            return IGNORE;
        case KEEP:
            return KEEP;
        default:
            throw new UnexpectedValueException(processing);
        }
    }
}