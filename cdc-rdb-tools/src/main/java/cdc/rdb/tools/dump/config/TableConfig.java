package cdc.rdb.tools.dump.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.rdb.RdbColumnSorting;

/**
 * Table configuration.
 *
 * @author Damien Carbonne
 *
 */
public final class TableConfig extends AbstractNamedConfig {
    final Map<String, ColumnConfig> columns = new HashMap<>();
    final List<RdbColumnSorting> sorting = new ArrayList<>();

    ColumnConfig register(ColumnConfig config) {
        columns.put(config.getInternal(), config);
        return config;
    }

    TableConfig(Builder builder) {
        super(builder);
    }

    TableConfig(SchemaConfig parent,
                String internal) {
        super(parent, internal);
        setProcessing(Processing.INHERIT);
    }

    @Override
    public SchemaConfig getParent() {
        return (SchemaConfig) super.getParent();
    }

    @Override
    public EffectiveProcessing getEffectiveProcessing() {
        return toEffectiveProcessing(columns.values());
    }

    public Set<String> getColumnsNames() {
        return columns.keySet();
    }

    public ColumnConfig getOrCreateColumnConfig(String columnName) {
        return columns.computeIfAbsent(columnName, s -> new ColumnConfig(this, s));
    }

    /**
     * Returns the configuration associated to a column.
     *
     * @param columnName The internal column name.
     * @return The configuration associated to {@code columnName} or null.
     */
    public ColumnConfig getColumnConfig(String columnName) {
        return columns.get(columnName);
    }

    public void addColumnSorting(RdbColumnSorting sorting) {
        this.sorting.add(sorting);
    }

    public List<RdbColumnSorting> getColumnsSorting() {
        return sorting;
    }

    public ColumnConfig.Builder column() {
        return new ColumnConfig.Builder(this);
    }

    public ColumnConfig.Builder column(String name) {
        return column().internal(name);
    }

    public static class Builder extends AbstractNamedConfig.Builder<TableConfig, Builder> {
        protected Builder(SchemaConfig parent) {
            super(parent);
            processing(Processing.INHERIT);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public TableConfig build() {
            return ((SchemaConfig) parent).register(new TableConfig(this));
        }

        public SchemaConfig back() {
            build();
            return (SchemaConfig) parent;
        }
    }
}