package cdc.rdb.tools.dump.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.converters.Converter;

/**
 * Dump configuration.
 * <p>
 * This can be used to select tables and columns to dump, the target name, ...
 *
 * @author Damien Carbonne
 *
 */
public final class DatabaseConfig extends AbstractConfig {
    private final Map<String, SchemaConfig> schemas = new HashMap<>();

    SchemaConfig register(SchemaConfig config) {
        schemas.put(config.getInternal(), config);
        return config;
    }

    protected DatabaseConfig(Builder builder) {
        super(builder);
    }

    public DatabaseConfig() {
        super((AbstractConfig) null);
        setProcessing(Processing.KEEP);
    }

    @Override
    public EffectiveProcessing getEffectiveProcessing() {
        return toEffectiveProcessing(schemas.values());
    }

    private static String validate(String s) {
        return s == null ? "" : s;
    }

    public Set<String> getSchemaNames() {
        return schemas.keySet();
    }

    public SchemaConfig getOrCreateSchemaConfig(String schemaName) {
        final String name = validate(schemaName);
        return schemas.computeIfAbsent(name, s -> new SchemaConfig(this, s));
    }

    /**
     * Returns the configuration associated to a schema.
     *
     * @param schemaName The schema name.
     * @return The configuration associated to {@code schemaName} or {@code null}.
     */
    public SchemaConfig getSchemaConfig(String schemaName) {
        return schemas.get(validate(schemaName));
    }

    /**
     * Returns the external name to use for a schema.
     * <p>
     * If an external name is explicitly defined for that schema, then returns it.<br>
     * Otherwise (no data related to that schema is found or external name is {@code null}), then returns {@code schemaName}.
     *
     * @param schemaName The internal schema name.
     * @return The external name to use for {@code schemaName}.
     */
    public String getExternalSchemaName(String schemaName) {
        final SchemaConfig schema = getSchemaConfig(schemaName);
        final String tmp = schema == null ? null : schema.getExternal();
        return tmp == null || tmp.isEmpty() ? schemaName : tmp;
    }

    public EffectiveProcessing getSchemaInheritedProcessing(String schemaName) {
        final SchemaConfig schema = getSchemaConfig(schemaName);
        return schema == null
                ? getInheritedProcessing()
                : schema.getInheritedProcessing();
    }

    public EffectiveProcessing getSchemaEffectiveProcessing(String schemaName) {
        final SchemaConfig schema = getSchemaConfig(schemaName);
        return schema == null
                ? getSchemaInheritedProcessing(schemaName)
                : schema.getEffectiveProcessing();
    }

    public boolean acceptsSchema(String schemaName) {
        return getSchemaEffectiveProcessing(schemaName) == EffectiveProcessing.KEEP;
    }

    public TableConfig getTableConfig(String schemaName,
                                      String tableName) {
        final SchemaConfig schema = getSchemaConfig(schemaName);
        return schema == null ? null : schema.getTableConfig(tableName);
    }

    /**
     * Returns the external name to use for a table.
     * <p>
     * If an external name is explicitly defined for that schema/table, then returns it.<br>
     * Otherwise (no data related to that schema/table is found or external name is {@code null}), then returns {@code tableName}.
     *
     * @param schemaName The schema name.
     * @param tableName The internal table name.
     * @return The external name corresponding to {@code tableName}.
     */
    public String getExternalTableName(String schemaName,
                                       String tableName) {
        final TableConfig table = getTableConfig(schemaName, tableName);
        final String tmp = table == null ? null : table.getExternal();
        return tmp == null || tmp.isEmpty() ? tableName : tmp;
    }

    public EffectiveProcessing getTableInheritedProcessing(String schemaName,
                                                           String tableName) {
        final TableConfig table = getTableConfig(schemaName, tableName);
        return table == null
                ? getSchemaInheritedProcessing(schemaName)
                : table.getInheritedProcessing();
    }

    public EffectiveProcessing getTableEffectiveProcessing(String schemaName,
                                                           String tableName) {
        final TableConfig table = getTableConfig(schemaName, tableName);
        return table == null
                ? getTableInheritedProcessing(schemaName, tableName)
                : table.getEffectiveProcessing();
    }

    public boolean acceptsTable(String schemaName,
                                String tableName) {
        return getTableEffectiveProcessing(schemaName, tableName) == EffectiveProcessing.KEEP;
    }

    public ColumnConfig getColumnConfig(String schemaName,
                                        String tableName,
                                        String columnName) {
        final TableConfig table = getTableConfig(schemaName, tableName);
        return table == null ? null : table.getColumnConfig(columnName);
    }

    /**
     * Returns the external name to use for a column.
     * <p>
     * If an external name is explicitly defined for that schema/table/column, then returns it.<br>
     * Otherwise (no data related to that schema/table/column is found or external name is {@code null}), then returns
     * {@code columnName}.
     *
     * @param schemaName The schema name.
     * @param tableName The internal table name.
     * @param columnName The internal column name.
     * @return The external name corresponding to {@code columnName}.
     */
    public String getExternalColumnName(String schemaName,
                                        String tableName,
                                        String columnName) {
        final ColumnConfig column = getColumnConfig(schemaName, tableName, columnName);
        final String tmp = column == null ? null : column.getExternal();
        return tmp == null || tmp.isEmpty() ? columnName : tmp;
    }

    public EffectiveProcessing getColumnInheritedProcessing(String schemaName,
                                                            String tableName,
                                                            String columnName) {
        final ColumnConfig column = getColumnConfig(schemaName, tableName, columnName);
        return column == null
                ? getTableInheritedProcessing(schemaName, tableName)
                : column.getInheritedProcessing();
    }

    /**
     * Returns {@code true} if a column can be dumped.
     * <p>
     * If no data related to that column is found, then dump is controlled by table.
     *
     * @param schemaName The schema name.
     * @param tableName The internal table name.
     * @param columnName The internal column name.
     * @return {@code true} if column named {@code columnName} can be dumped.
     */
    public boolean acceptsColumn(String schemaName,
                                 String tableName,
                                 String columnName) {
        return getColumnInheritedProcessing(schemaName, tableName, columnName) == EffectiveProcessing.KEEP;
    }

    public Converter<?, ?> getConverter(String schemaName,
                                        String tableName,
                                        String columnName) {
        final ColumnConfig column = getColumnConfig(schemaName, tableName, columnName);
        return column == null ? null : column.getConverter();
    }

    public static Builder builder() {
        return new Builder();
    }

    public SchemaConfig.Builder schema() {
        return new SchemaConfig.Builder(this);
    }

    public SchemaConfig.Builder schema(String name) {
        return schema().internal(name);
    }

    public static class Builder extends AbstractConfig.Builder<DatabaseConfig, Builder> {
        protected Builder() {
            super(null);
            processing(Processing.KEEP);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public DatabaseConfig build() {
            return new DatabaseConfig(this);
        }
    }
}