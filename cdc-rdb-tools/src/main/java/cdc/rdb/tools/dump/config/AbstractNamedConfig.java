package cdc.rdb.tools.dump.config;

import cdc.util.lang.Checks;

/**
 * Base abstract configuration of objects that have an internal name.
 * <p>
 * An optional external name can be associated.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractNamedConfig extends AbstractConfig {
    /** Internal name. */
    private final String internal;

    /** External name. */
    private String external;

    AbstractNamedConfig(Builder<?, ?> builder) {
        super(builder);
        this.internal = Checks.isNotNull(builder.internal, "internal");
        this.external = builder.external;
    }

    AbstractNamedConfig(AbstractConfig parent,
                        String internal) {
        super(parent);
        this.internal = Checks.isNotNull(internal, "internal");
    }

    /**
     * @return The internal name.
     */
    public final String getInternal() {
        return internal;
    }

    /**
     * @return The external name.
     */
    public final String getExternal() {
        return external;
    }

    public final void setExternal(String external) {
        this.external = external;
    }

    /**
     * @return The effective external name.
     *         It is the external name if it is not {@code null},
     *         or the internal name.
     */

    public final String getEffectiveExternal() {
        return external == null ? internal : external;
    }

    public abstract static class Builder<C extends AbstractNamedConfig, B extends Builder<C, B>>
            extends AbstractConfig.Builder<C, B> {
        private String internal;
        private String external;

        protected Builder(AbstractConfig parent) {
            super(parent);
        }

        public B internal(String internal) {
            this.internal = internal;
            return self();
        }

        public B external(String external) {
            this.external = external;
            return self();
        }
    }
}