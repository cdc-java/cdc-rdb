package cdc.rdb.tools.dump;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.xml.XmlWriter;
import cdc.rdb.tools.dump.RdbDump.MainArgs;
import cdc.rdb.tools.dump.RdbDump.MainArgs.Feature;

/**
 * Handler dedicated to XML.
 *
 * @author Damien Carbonne
 */
final class XmlHandler extends AbstractHandler {
    private static final Logger LOGGER = LogManager.getLogger(XmlHandler.class);
    private XmlWriter xmlWriter = null;

    public XmlHandler(RdbDump.MainArgs margs) {
        super(margs);
    }

    private boolean acceptsValue(String s) {
        return !margs.isEnabled(Feature.NO_EMPTY_VALUES) || s != null && !s.isEmpty();
    }

    @Override
    public String getExtension() {
        return "xml";
    }

    @Override
    public void startTable(String basename,
                           String tableName) throws IOException {
        final File file = getFile(basename);
        if (margs.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info("Create {}", file);
        }
        xmlWriter = new XmlWriter(file);
        xmlWriter.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
        xmlWriter.setTabSize(2);
        xmlWriter.beginDocument();
        xmlWriter.beginElement("table");
        xmlWriter.addAttribute("name", tableName);
    }

    @Override
    public void header(List<String> columnNames) {
        // Ignore
    }

    @Override
    public void startRow(int rowIndex) throws IOException {
        xmlWriter.beginElement("row");

    }

    @Override
    public void column(String name,
                       String value) throws IOException {
        if (acceptsValue(value)) {
            if (margs.isEnabled(Feature.SPECIFIC_ATTRIBUTES)) {
                xmlWriter.addAttribute(name, value);
            } else if (margs.isEnabled(Feature.GENERIC_ELEMENTS)) {
                xmlWriter.beginElement("column");
                xmlWriter.addAttribute("name", name);
                xmlWriter.addElementContent(value);
                xmlWriter.endElement();
            } else {
                xmlWriter.beginElement(name);
                xmlWriter.addElementContent(value);
                xmlWriter.endElement();
            }
        }
    }

    @Override
    public void endRow() throws IOException {
        xmlWriter.endElement();
    }

    @Override
    public void endTable() throws IOException {
        xmlWriter.endElement();
        xmlWriter.endDocument();
        xmlWriter.close();
    }
}