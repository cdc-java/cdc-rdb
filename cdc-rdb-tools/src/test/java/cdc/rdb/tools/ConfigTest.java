package cdc.rdb.tools;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.rdb.tools.dump.config.ColumnConfig;
import cdc.rdb.tools.dump.config.DatabaseConfig;
import cdc.rdb.tools.dump.config.EffectiveProcessing;
import cdc.rdb.tools.dump.config.Processing;
import cdc.rdb.tools.dump.config.SchemaConfig;
import cdc.rdb.tools.dump.config.TableConfig;

class ConfigTest {
    private static final String SCHEMA1 = "schema1";
    private static final String SCHEMA2 = "schema2";
    private static final String TABLE1 = "table1";
    private static final String TABLE2 = "table2";
    private static final String COLUMN1 = "col1";
    private static final String COLUMN2 = "col2";

    @Test
    void testProcessing() {
        // Create with default settings
        final DatabaseConfig database = new DatabaseConfig();
        assertEquals(Processing.KEEP, database.getProcessing());
        assertEquals(EffectiveProcessing.KEEP, database.getInheritedProcessing());
        assertEquals(EffectiveProcessing.KEEP, database.getEffectiveProcessing());

        assertTrue(database.acceptsSchema("schema"));
        assertTrue(database.acceptsTable("schema", "table"));
        assertTrue(database.acceptsColumn("schema", "table", "column1"));
        assertTrue(database.acceptsColumn("schema", "table", "column2"));
        assertTrue(database.acceptsColumn("schema", "table", "column3"));

        final SchemaConfig schema = database.getOrCreateSchemaConfig("schema");
        assertEquals(Processing.INHERIT, schema.getProcessing());
        assertEquals(EffectiveProcessing.KEEP, schema.getInheritedProcessing());
        assertEquals(EffectiveProcessing.KEEP, schema.getEffectiveProcessing());

        final TableConfig table = schema.getOrCreateTableConfig("table");
        assertEquals(Processing.INHERIT, table.getProcessing());
        assertEquals(EffectiveProcessing.KEEP, table.getInheritedProcessing());
        assertEquals(EffectiveProcessing.KEEP, table.getEffectiveProcessing());

        final ColumnConfig column1 = table.getOrCreateColumnConfig("column1");
        assertEquals(Processing.INHERIT, column1.getProcessing());
        assertEquals(EffectiveProcessing.KEEP, column1.getInheritedProcessing());
        assertEquals(EffectiveProcessing.KEEP, column1.getEffectiveProcessing());

        final ColumnConfig column2 = table.getOrCreateColumnConfig("column2");
        assertEquals(Processing.INHERIT, column2.getProcessing());
        assertEquals(EffectiveProcessing.KEEP, column2.getInheritedProcessing());
        assertEquals(EffectiveProcessing.KEEP, column2.getEffectiveProcessing());

        // Change database processing
        database.setProcessing(Processing.IGNORE);

        assertFalse(database.acceptsSchema("schema"));
        assertFalse(database.acceptsTable("schema", "table"));
        assertFalse(database.acceptsColumn("schema", "table", "column1"));
        assertFalse(database.acceptsColumn("schema", "table", "column2"));
        assertFalse(database.acceptsColumn("schema", "table", "column3"));

        assertEquals(Processing.IGNORE, database.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, database.getInheritedProcessing());
        assertEquals(EffectiveProcessing.IGNORE, database.getEffectiveProcessing());

        assertEquals(Processing.INHERIT, schema.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, schema.getInheritedProcessing());
        assertEquals(EffectiveProcessing.IGNORE, schema.getEffectiveProcessing());

        assertEquals(Processing.INHERIT, table.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, table.getInheritedProcessing());
        assertEquals(EffectiveProcessing.IGNORE, table.getEffectiveProcessing());

        assertEquals(Processing.INHERIT, column1.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, column1.getInheritedProcessing());
        assertEquals(EffectiveProcessing.IGNORE, column1.getEffectiveProcessing());

        assertEquals(Processing.INHERIT, column2.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, column2.getInheritedProcessing());
        assertEquals(EffectiveProcessing.IGNORE, column2.getEffectiveProcessing());

        // Change column1 processing
        column1.setProcessing(Processing.KEEP);

        assertTrue(database.acceptsSchema("schema"));
        assertTrue(database.acceptsTable("schema", "table"));
        assertTrue(database.acceptsColumn("schema", "table", "column1"));
        assertFalse(database.acceptsColumn("schema", "table", "column2"));
        assertFalse(database.acceptsColumn("schema", "table", "column3"));

        assertEquals(Processing.IGNORE, database.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, database.getInheritedProcessing());
        assertEquals(EffectiveProcessing.KEEP, database.getEffectiveProcessing());

        assertEquals(Processing.INHERIT, schema.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, schema.getInheritedProcessing());
        assertEquals(EffectiveProcessing.KEEP, schema.getEffectiveProcessing());

        assertEquals(Processing.INHERIT, table.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, table.getInheritedProcessing());
        assertEquals(EffectiveProcessing.KEEP, table.getEffectiveProcessing());

        assertEquals(Processing.KEEP, column1.getProcessing());
        assertEquals(EffectiveProcessing.KEEP, column1.getInheritedProcessing());
        assertEquals(EffectiveProcessing.KEEP, column1.getEffectiveProcessing());

        assertEquals(Processing.INHERIT, column2.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, column2.getInheritedProcessing());
        assertEquals(EffectiveProcessing.IGNORE, column2.getEffectiveProcessing());
    }

    @Test
    void testProcessingInheritRoot() {
        final DatabaseConfig database = new DatabaseConfig();
        assertThrows(IllegalArgumentException.class,
                     () -> database.setProcessing(Processing.INHERIT));
    }

    @Test
    void testProcessingDefault() {
        // Create with default settings
        final DatabaseConfig database = new DatabaseConfig();
        assertEquals(Processing.KEEP, database.getProcessing());
        assertEquals(EffectiveProcessing.KEEP, database.getInheritedProcessing());
        assertEquals(EffectiveProcessing.KEEP, database.getEffectiveProcessing());

        assertTrue(database.acceptsSchema("schema"));
        assertTrue(database.acceptsTable("schema", "table"));
        assertTrue(database.acceptsColumn("schema", "table", "column1"));
        assertTrue(database.acceptsColumn("schema", "table", "column2"));
        assertTrue(database.acceptsColumn("schema", "table", "column3"));
    }

    @Test
    void testProcessingIgnore() {
        final DatabaseConfig database = new DatabaseConfig();
        database.setProcessing(Processing.IGNORE);
        assertEquals(Processing.IGNORE, database.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, database.getInheritedProcessing());
        assertEquals(EffectiveProcessing.IGNORE, database.getEffectiveProcessing());

        assertFalse(database.acceptsSchema("schema"));
        assertFalse(database.acceptsTable("schema", "table"));
        assertFalse(database.acceptsColumn("schema", "table", "column1"));
        assertFalse(database.acceptsColumn("schema", "table", "column2"));
        assertFalse(database.acceptsColumn("schema", "table", "column3"));
    }

    @Test
    void testProcessingIgnoreKeep() {
        final DatabaseConfig database = new DatabaseConfig();
        database.setProcessing(Processing.IGNORE);
        assertEquals(Processing.IGNORE, database.getProcessing());
        assertEquals(EffectiveProcessing.IGNORE, database.getInheritedProcessing());
        assertEquals(EffectiveProcessing.IGNORE, database.getEffectiveProcessing());
        database.getOrCreateSchemaConfig("schema").setProcessing(Processing.KEEP);

        assertTrue(database.acceptsSchema("schema"));
        assertTrue(database.acceptsTable("schema", "table"));
        assertTrue(database.acceptsColumn("schema", "table", "column1"));
        assertTrue(database.acceptsColumn("schema", "table", "column2"));
        assertTrue(database.acceptsColumn("schema", "table", "column3"));
    }

    @Test
    void testProcessing1() {
        final DatabaseConfig database = new DatabaseConfig();
        database.setProcessing(Processing.IGNORE);
        final SchemaConfig schema1 = database.getOrCreateSchemaConfig(SCHEMA1);
        schema1.setProcessing(Processing.KEEP);
        final SchemaConfig schema2 = database.getOrCreateSchemaConfig(SCHEMA2);
        schema2.setProcessing(Processing.IGNORE);
        assertTrue(database.acceptsSchema(SCHEMA1));
        assertTrue(database.acceptsTable(SCHEMA1, TABLE1));
        assertTrue(database.acceptsColumn(SCHEMA1, TABLE1, COLUMN1));
        assertTrue(database.acceptsColumn(SCHEMA1, TABLE1, COLUMN2));
        assertTrue(database.acceptsTable(SCHEMA1, TABLE2));
        assertTrue(database.acceptsColumn(SCHEMA1, TABLE2, COLUMN1));
        assertTrue(database.acceptsColumn(SCHEMA1, TABLE2, COLUMN2));
        assertFalse(database.acceptsSchema(SCHEMA2));
        assertFalse(database.acceptsTable(SCHEMA2, TABLE1));
        assertFalse(database.acceptsColumn(SCHEMA2, TABLE1, COLUMN1));
        assertFalse(database.acceptsColumn(SCHEMA2, TABLE1, COLUMN2));
        assertFalse(database.acceptsTable(SCHEMA2, TABLE2));
        assertFalse(database.acceptsColumn(SCHEMA2, TABLE2, COLUMN1));
        assertFalse(database.acceptsColumn(SCHEMA2, TABLE2, COLUMN2));
    }

    @Test
    void testProcessing2() {
        final DatabaseConfig database = new DatabaseConfig();
        database.setProcessing(Processing.IGNORE);
        final SchemaConfig schema1 = database.getOrCreateSchemaConfig(SCHEMA1);
        final TableConfig table11 = schema1.getOrCreateTableConfig(TABLE1);
        table11.setProcessing(Processing.KEEP);
        schema1.setProcessing(Processing.IGNORE);
        final SchemaConfig schema2 = database.getOrCreateSchemaConfig(SCHEMA2);
        schema2.setProcessing(Processing.IGNORE);
        assertTrue(database.acceptsSchema(SCHEMA1));
        assertTrue(database.acceptsTable(SCHEMA1, TABLE1));
        assertTrue(database.acceptsColumn(SCHEMA1, TABLE1, COLUMN1));
        assertTrue(database.acceptsColumn(SCHEMA1, TABLE1, COLUMN2));
        assertFalse(database.acceptsTable(SCHEMA1, TABLE2));
        assertFalse(database.acceptsColumn(SCHEMA1, TABLE2, COLUMN1));
        assertFalse(database.acceptsColumn(SCHEMA1, TABLE2, COLUMN2));
        assertFalse(database.acceptsSchema(SCHEMA2));
        assertFalse(database.acceptsTable(SCHEMA2, TABLE1));
        assertFalse(database.acceptsColumn(SCHEMA2, TABLE1, COLUMN1));
        assertFalse(database.acceptsColumn(SCHEMA2, TABLE1, COLUMN2));
        assertFalse(database.acceptsTable(SCHEMA2, TABLE2));
        assertFalse(database.acceptsColumn(SCHEMA2, TABLE2, COLUMN1));
        assertFalse(database.acceptsColumn(SCHEMA2, TABLE2, COLUMN2));
    }

    @Test
    void testExternalNames() {
        final DatabaseConfig database = new DatabaseConfig();
        assertEquals("schema", database.getExternalSchemaName("schema"));
        assertEquals("table", database.getExternalTableName("schema", "table"));
        assertEquals("column", database.getExternalColumnName("schema", "table", "column"));

        final SchemaConfig schema = database.getOrCreateSchemaConfig("schema");
        schema.setExternal("SCHEMA");
        assertEquals("SCHEMA", database.getExternalSchemaName("schema"));
        assertEquals("table", database.getExternalTableName("schema", "table"));
        assertEquals("column", database.getExternalColumnName("schema", "table", "column"));

        final TableConfig table = schema.getOrCreateTableConfig("table");
        table.setExternal("TABLE");
        assertEquals("SCHEMA", database.getExternalSchemaName("schema"));
        assertEquals("TABLE", database.getExternalTableName("schema", "table"));
        assertEquals("column", database.getExternalColumnName("schema", "table", "column"));

        final ColumnConfig column = table.getOrCreateColumnConfig("column");
        column.setExternal("COLUMN");
        assertEquals("SCHEMA", database.getExternalSchemaName("schema"));
        assertEquals("TABLE", database.getExternalTableName("schema", "table"));
        assertEquals("COLUMN", database.getExternalColumnName("schema", "table", "column"));
    }
}