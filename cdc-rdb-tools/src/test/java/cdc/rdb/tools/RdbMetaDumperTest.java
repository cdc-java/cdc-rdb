package cdc.rdb.tools;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.util.cli.MainResult;

class RdbMetaDumperTest {
    @Test
    void test() {
        final MainResult result =
                RdbMetaDumper.exec("--dump-all",
                                   "--url",
                                   "jdbc:ucanaccess://../cdc-rdb-core/src/test/resources/Assets4_Data.mdb;showSchema=true",
                                   "--output",
                                   "target/assets4-dump.xlsx",
                                   "--verbose");
        assertSame(MainResult.SUCCESS, result);
    }
}