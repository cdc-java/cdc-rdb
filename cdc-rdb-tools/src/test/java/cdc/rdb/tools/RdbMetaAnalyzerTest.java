package cdc.rdb.tools;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.util.cli.MainResult;

class RdbMetaAnalyzerTest {
    @Test
    void testJdbc() {
        final MainResult result =
                RdbMetaAnalyzer.exec("--url",
                                     "jdbc:ucanaccess://../cdc-rdb-core/src/test/resources/Assets4_Data.mdb;showSchema=true",
                                     // "--driver",
                                     // "net.ucanaccess.jdbc.UcanaccessDriver",
                                     "--xml",
                                     "target/assets4-analyzer-jdbc.xml");
        assertSame(MainResult.SUCCESS, result);
    }

    @Test
    void testJackcess() {
        final MainResult result =
                RdbMetaAnalyzer.exec("--jackcess",
                                     "--url",
                                     "../cdc-rdb-core/src/test/resources/Assets4_Data.mdb",
                                     "--xml",
                                     "target/assets4-analyzer-jackcess.xml");
        assertSame(MainResult.SUCCESS, result);
    }
}