package cdc.rdb;

/**
 * Index column description.
 * <p>
 * Its parent is an Index.<br>
 * Its name must be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbIndexColumn extends RdbChildElement<RdbIndex> {
    public static final String KIND = KIND_INDEX_COLUMN;

    private final short ordinal;

    private RdbIndexColumn(Builder builder) {
        super(builder, false);
        this.ordinal = builder.ordinal;
    }

    public RdbIndex getIndex() {
        return getParent();
    }

    public short getOrdinal() {
        return ordinal;
    }

    public RdbTableColumn getColumn() {
        final RdbTable table = getParent().getParent();
        return table == null ? null : table.getOptionalColumn(name);
    }

    static Builder builder(RdbIndex parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbIndex> {
        private short ordinal = -1;

        private Builder(RdbIndex parent) {
            super(parent);
        }

        public Builder ordinal(short ordinal) {
            this.ordinal = ordinal;
            return self();
        }

        @Override
        public RdbIndexColumn build() {
            return new RdbIndexColumn(this);
        }
    }
}