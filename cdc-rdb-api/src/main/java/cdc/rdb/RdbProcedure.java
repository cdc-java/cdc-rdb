package cdc.rdb;

/**
 * Procedure description.
 * <p>
 * Its parent is a Schema.<br>
 * Its name must NOT be unique.<br>
 * Its content is:
 * <ul>
 * <li>Procedure columns
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbProcedure extends RdbChildElement<RdbSchema> {
    public static final String KIND = KIND_PROCEDURE;

    private final String specificName;
    private final ProcedureResultType resultType;

    RdbProcedure(Builder builder) {
        super(builder, true);
        this.specificName = builder.specificName;
        this.resultType = builder.resultType;
    }

    public RdbProcedureColumn.Builder column() {
        return RdbProcedureColumn.builder(this);
    }

    public Iterable<RdbProcedureColumn> getColumns() {
        return getChildren(RdbProcedureColumn.class);
    }

    public RdbProcedureColumn getOptionalColumn(String name) {
        return getFirstChild(RdbProcedureColumn.class, name);
    }

    public RdbProcedureColumn getColumn(String name) {
        return notNull(getOptionalColumn(name), "procedure column", name);
    }

    public String getSpecificName() {
        return specificName;
    }

    public ProcedureResultType getResultType() {
        return resultType;
    }

    static Builder builder(RdbSchema parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbSchema> {
        private String specificName;
        private ProcedureResultType resultType;

        private Builder(RdbSchema parent) {
            super(parent);
        }

        public Builder specificName(String specificName) {
            this.specificName = specificName;
            return self();
        }

        public Builder resultType(ProcedureResultType resultType) {
            this.resultType = resultType;
            return self();
        }

        @Override
        public RdbProcedure build() {
            return new RdbProcedure(this);
        }
    }
}