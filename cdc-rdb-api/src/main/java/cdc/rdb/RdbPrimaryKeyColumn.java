package cdc.rdb;

/**
 * Primary key column description.
 * <p>
 * Its parent is a Primary key.<br>
 * Its name must be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbPrimaryKeyColumn extends RdbChildElement<RdbPrimaryKey> {
    public static final String KIND = KIND_PRIMARY_KEY_COLUMN;

    private final short ordinal;

    protected RdbPrimaryKeyColumn(Builder builder) {
        super(builder, false);
        this.ordinal = builder.ordinal;
    }

    public short getOrdinal() {
        return ordinal;
    }

    public RdbTableColumn getColumn() {
        return getParent().getParent().getOptionalColumn(getName());
    }

    static Builder builder(RdbPrimaryKey parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbPrimaryKey> {
        private short ordinal;

        private Builder(RdbPrimaryKey parent) {
            super(parent);
        }

        public Builder ordinal(short ordinal) {
            this.ordinal = ordinal;
            return self();
        }

        @Override
        public RdbPrimaryKeyColumn build() {
            return new RdbPrimaryKeyColumn(this);
        }
    }
}