package cdc.rdb;

/**
 * Primary key description.
 * <p>
 * Its parent is a Table.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Primary key columns
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbPrimaryKey extends RdbChildElement<RdbTable> {
    public static final String KIND = KIND_PRIMARY_KEY;

    private RdbPrimaryKey(Builder builder) {
        super(builder, false);
    }

    public RdbPrimaryKeyColumn.Builder column() {
        return RdbPrimaryKeyColumn.builder(this);
    }

    public RdbPrimaryKeyColumn getOptionalColumn(String name) {
        return getFirstChild(RdbPrimaryKeyColumn.class, name);
    }

    public RdbPrimaryKeyColumn getColumn(String name) {
        return notNull(getOptionalColumn(name), "primary key column", name);
    }

    public Iterable<RdbPrimaryKeyColumn> getColumns() {
        return getChildren(RdbPrimaryKeyColumn.class);
    }

    static Builder builder(RdbTable parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbTable> {
        protected Builder(RdbTable parent) {
            super(parent);
        }

        @Override
        public RdbPrimaryKey build() {
            return new RdbPrimaryKey(this);
        }
    }
}