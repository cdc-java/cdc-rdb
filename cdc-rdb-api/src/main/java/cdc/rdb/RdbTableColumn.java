package cdc.rdb;

/**
 * Table column description.
 * <p>
 * Its parent is a Table.<br>
 * Its name must be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbTableColumn extends RdbChildElement<RdbTable> {
    public static final String KIND = KIND_TABLE_COLUMN;

    private final SqlDataType dataType;
    private final String typeName;
    private final int size;
    private final int digits;
    private final int radix;
    private final YesNoUnknown nullable;
    private final String defaultValue;
    private final int ordinal;
    private final YesNoUnknown autoIncrement;
    private final YesNoUnknown generated;

    private RdbTableColumn(Builder builder) {
        super(builder, false);
        this.dataType = builder.dataType;
        this.typeName = builder.typeName;
        this.size = builder.size;
        this.digits = builder.digits;
        this.radix = builder.radix;
        this.nullable = builder.nullable;
        this.defaultValue = builder.defaultValue;
        this.ordinal = builder.ordinal;
        this.autoIncrement = builder.autoIncrement;
        this.generated = builder.generated;
    }

    public SqlDataType getDataType() {
        return dataType;
    }

    public String getTypeName() {
        return typeName;
    }

    public int getSize() {
        return size;
    }

    public int getDigits() {
        return digits;
    }

    public int getRadix() {
        return radix;
    }

    public YesNoUnknown getNullable() {
        return nullable;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public YesNoUnknown getAutoIncrement() {
        return autoIncrement;
    }

    public YesNoUnknown getGenerated() {
        return generated;
    }

    public boolean isPrimaryKey() {
        final RdbTable table = getParent();
        for (final RdbPrimaryKey pk : table.getChildren(RdbPrimaryKey.class)) {
            for (final RdbPrimaryKeyColumn column : pk.getChildren(RdbPrimaryKeyColumn.class)) {
                if (column.getName().equals(getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isForeignKey() {
        final RdbTable table = getParent();
        for (final RdbForeignKey fk : table.getChildren(RdbForeignKey.class)) {
            for (final RdbForeignKeyColumn column : fk.getChildren(RdbForeignKeyColumn.class)) {
                if (column.getName().equals(getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    static Builder builder(RdbTable parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbTable> {
        private SqlDataType dataType;
        private String typeName = "";
        private int size = -1;
        private int digits = -1;
        private int radix = -1;
        private YesNoUnknown nullable = YesNoUnknown.UNKNOWN;
        private String defaultValue;
        private int ordinal = -1;
        private YesNoUnknown autoIncrement = YesNoUnknown.UNKNOWN;
        private YesNoUnknown generated = YesNoUnknown.UNKNOWN;

        private Builder(RdbTable parent) {
            super(parent);
        }

        public Builder dataType(SqlDataType dataType) {
            this.dataType = dataType;
            return self();
        }

        public Builder typeName(String typeName) {
            this.typeName = typeName;
            return self();
        }

        public Builder size(int size) {
            this.size = size;
            return self();
        }

        public Builder digits(int digits) {
            this.digits = digits;
            return self();
        }

        public Builder radix(int radix) {
            this.radix = radix;
            return self();
        }

        public Builder nullable(YesNoUnknown nullable) {
            this.nullable = nullable;
            return self();
        }

        public Builder defaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
            return self();
        }

        public Builder ordinal(int ordinal) {
            this.ordinal = ordinal;
            return self();
        }

        public Builder autoIncrement(YesNoUnknown autoIncrement) {
            this.autoIncrement = autoIncrement;
            return self();
        }

        public Builder generated(YesNoUnknown generated) {
            this.generated = generated;
            return self();
        }

        @Override
        public RdbTableColumn build() {
            return new RdbTableColumn(this);
        }
    }
}