package cdc.rdb;

public enum RdbColumnOrder {
    ASCENDING,
    DESCENDING
}