package cdc.rdb;

/**
 * Schema description.
 * <p>
 * Its parent is a Catalog.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Tables
 * <li>User data types
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbSchema extends RdbChildElement<RdbCatalog> {
    public static final String KIND = KIND_SCHEMA;

    private RdbSchema(Builder builder) {
        super(builder, false);
    }

    public RdbCatalog getCatalog() {
        return getParent();
    }

    public RdbDatabase getDatabase() {
        return getCatalog().getDatabase();
    }

    public RdbTable.Builder table() {
        return RdbTable.builder(this);
    }

    public RdbTable getOptionalTable(String name) {
        return getFirstChild(RdbTable.class, name);
    }

    public RdbTable getTable(String name) {
        return notNull(getOptionalTable(name), "table", name);
    }

    public Iterable<RdbTable> getTables() {
        return getChildren(RdbTable.class);
    }

    public RdbUserDataType.Builder userDataType() {
        return RdbUserDataType.builder(this);
    }

    public RdbUserDataType getOptionalUserDataType(String name) {
        return getFirstChild(RdbUserDataType.class, name);
    }

    public RdbUserDataType getUserDataType(String name) {
        return notNull(getOptionalUserDataType(name), "user data type", name);
    }

    public Iterable<RdbUserDataType> getUserDataTypes() {
        return getChildren(RdbUserDataType.class);
    }

    public RdbFunction.Builder function() {
        return RdbFunction.builder(this);
    }

    public RdbFunction getOptionalFunction(String specificName) {
        for (final RdbFunction child : getFunctions()) {
            if (specificName.equals(child.getSpecificName())) {
                return child;
            }
        }
        return null;
    }

    public RdbFunction getFunction(String specificName) {
        return notNull(getOptionalFunction(specificName), "function", specificName);
    }

    public Iterable<RdbFunction> getFunctions() {
        return getChildren(RdbFunction.class);
    }

    public RdbProcedure.Builder procedure() {
        return RdbProcedure.builder(this);
    }

    public RdbProcedure getOptionalProcedure(String specificName) {
        for (final RdbProcedure child : getProcedures()) {
            if (specificName.equals(child.getSpecificName())) {
                return child;
            }
        }
        return null;
    }

    public RdbProcedure getProcedure(String specificName) {
        return notNull(getOptionalProcedure(specificName), "procedure", specificName);
    }

    public Iterable<RdbProcedure> getProcedures() {
        return getChildren(RdbProcedure.class);
    }

    static Builder builder(RdbCatalog parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbCatalog> {
        private Builder(RdbCatalog parent) {
            super(parent);
        }

        @Override
        public RdbSchema build() {
            return new RdbSchema(this);
        }
    }
}