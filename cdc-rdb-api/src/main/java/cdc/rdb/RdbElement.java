package cdc.rdb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.util.function.IterableUtils;
import cdc.util.lang.NotFoundException;
import cdc.util.lang.Operators;

/**
 * Base abstract class of elements.
 *
 * @author Damien Carbonne
 *
 */
public abstract class RdbElement {
    protected final String name;
    private final String comments;
    protected final List<RdbElement> children = new ArrayList<>();

    public static final String KIND_ATTRIBUTE = "ATTRIBUTE";
    public static final String KIND_CATALOG = "CATALOG";
    public static final String KIND_DATABASE = "DATABASE";
    public static final String KIND_DATA_TYPE = "DATA_TYPE";
    public static final String KIND_FOREIGN_KEY = "FOREIGN_KEY";
    public static final String KIND_FOREIGN_KEY_COLUMN = "FOREIGN_KEY_COLUMN";
    public static final String KIND_FUNCTION = "FUNCTION";
    public static final String KIND_FUNCTION_COLUMN = "FUNCTION_COLUMN";
    public static final String KIND_INDEX = "INDEX";
    public static final String KIND_INDEX_COLUMN = "INDEX_COLUMN";
    public static final String KIND_PRIMARY_KEY = "PRIMARY_KEY";
    public static final String KIND_PRIMARY_KEY_COLUMN = "PRIMARY_KEY_COLUMN";
    public static final String KIND_PROCEDURE = "PROCEDURE";
    public static final String KIND_PROCEDURE_COLUMN = "PROCEDURE_COLUMN";
    public static final String KIND_SCHEMA = "SCHEMA";
    public static final String KIND_TABLE = "TABLE";
    public static final String KIND_TABLE_COLUMN = "TABLE_COLUMN";
    public static final String KIND_TABLE_TYPE = "TABLE_TYPE";
    public static final String KIND_USER_DATA_TYPE = "USER_DATA_TYPE";

    private record Bucket(Class<? extends RdbElement> elementClass,
                          String kind,
                          Class<? extends RdbElement> parentClass,
                          int depth) {
    }

    private static final Map<Class<? extends RdbElement>, Bucket> BUCKETS = new HashMap<>();
    private static final Map<String, Class<? extends RdbElement>> KIND_TO_CLASS = new HashMap<>();

    private static void add(Class<? extends RdbElement> elementClass,
                            String kind,
                            Class<? extends RdbElement> parentClass,
                            int depth) {
        BUCKETS.put(elementClass, new Bucket(elementClass, kind, parentClass, depth));
        KIND_TO_CLASS.put(kind, elementClass);
    }

    static {
        add(RdbDatabase.class, KIND_DATABASE, null, 1);

        add(RdbCatalog.class, KIND_CATALOG, RdbDatabase.class, 2);
        add(RdbDataType.class, KIND_DATA_TYPE, RdbDatabase.class, 2);
        add(RdbTableType.class, KIND_TABLE_TYPE, RdbDatabase.class, 2);

        add(RdbSchema.class, KIND_SCHEMA, RdbCatalog.class, 3);

        add(RdbFunction.class, KIND_FUNCTION, RdbSchema.class, 4);
        add(RdbProcedure.class, KIND_PROCEDURE, RdbSchema.class, 4);
        add(RdbTable.class, KIND_TABLE, RdbSchema.class, 4);
        add(RdbUserDataType.class, KIND_USER_DATA_TYPE, RdbSchema.class, 4);

        add(RdbFunctionColumn.class, KIND_FUNCTION_COLUMN, RdbFunction.class, 5);
        add(RdbProcedureColumn.class, KIND_PROCEDURE_COLUMN, RdbProcedure.class, 5);
        add(RdbTableColumn.class, KIND_TABLE_COLUMN, RdbTable.class, 5);
        add(RdbForeignKey.class, KIND_FOREIGN_KEY, RdbTable.class, 5);
        add(RdbIndex.class, KIND_INDEX, RdbTable.class, 5);
        add(RdbPrimaryKey.class, KIND_PRIMARY_KEY, RdbTable.class, 5);
        add(RdbAttribute.class, KIND_ATTRIBUTE, RdbUserDataType.class, 5);

        add(RdbForeignKeyColumn.class, KIND_FOREIGN_KEY_COLUMN, RdbForeignKey.class, 6);
        add(RdbIndexColumn.class, KIND_INDEX_COLUMN, RdbIndex.class, 6);
        add(RdbPrimaryKeyColumn.class, KIND_PRIMARY_KEY_COLUMN, RdbPrimaryKey.class, 6);
    }

    public static Set<Class<? extends RdbElement>> getElementClasses() {
        return BUCKETS.keySet();
    }

    public static String getKind(Class<? extends RdbElement> elementClass) {
        return BUCKETS.containsKey(elementClass)
                ? BUCKETS.get(elementClass).kind
                : null;
    }

    public static Class<? extends RdbElement> getElementClass(String kind) {
        return KIND_TO_CLASS.get(kind);
    }

    public static int getDepth(Class<? extends RdbElement> elementClass) {
        return BUCKETS.containsKey(elementClass)
                ? BUCKETS.get(elementClass).depth
                : -1;
    }

    public static Class<? extends RdbElement> getParentClass(Class<? extends RdbElement> elementClass) {
        return BUCKETS.containsKey(elementClass)
                ? BUCKETS.get(elementClass).parentClass
                : null;
    }

    protected RdbElement(Builder<?> builder) {
        this.name = validate(builder.name);
        this.comments = builder.comments;
    }

    private static String validate(String name) {
        return name == null ? "" : name;
    }

    protected static <E> E notNull(E value,
                                   String type,
                                   String name) {
        if (value == null) {
            throw new NotFoundException("Could not find " + type + " '" + name + "'");
        } else {
            return value;
        }
    }

    public final String getKind() {
        return getKind(getClass());
    }

    public final String getName() {
        return name;
    }

    public abstract RdbElementPath getPath();

    public abstract RdbElement getParent();

    public final int getDepth() {
        int result = 0;
        RdbElement index = this;
        while (index != null) {
            index = index.getParent();
            result++;
        }
        return result;
    }

    public final String getComments() {
        return comments;
    }

    public final Iterable<RdbElement> getChildren() {
        return children;
    }

    public final <T extends RdbElement> Iterable<T> getChildren(Class<T> childClass) {
        return IterableUtils.convert(childClass, children);
    }

    public final <T extends RdbElement> int getChildrenCount(Class<T> childClass) {
        return IterableUtils.size(getChildren(childClass));
    }

    public final <T extends RdbElement> boolean hasChildren(Class<T> childClass) {
        return !IterableUtils.isEmpty(getChildren(childClass));
    }

    public final <T extends RdbElement> T getFirstChild(Class<T> childClass,
                                                        String name) {
        final String n = validate(name);
        for (final T child : getChildren(childClass)) {
            if (Operators.equals(n, child.getName())) {
                return child;
            }
        }
        return null;
    }

    public final <T extends RdbElement> int getChildrenCount(Class<T> childClass,
                                                             String name) {
        final String n = validate(name);
        int result = 0;
        for (final T child : getChildren(childClass)) {
            if (Operators.equals(n, child.getName())) {
                result++;
            }
        }
        return result;
    }

    public final <T extends RdbElement> List<T> getChildren(Class<T> childClass,
                                                            String name) {
        final String n = validate(name);
        final List<T> result = new ArrayList<>();
        for (final T child : getChildren(childClass)) {
            if (Operators.equals(n, child.getName())) {
                result.add(child);
            }
        }
        return result;
    }

    public final <T extends RdbElement> T getFirstChild(Class<T> childClass) {
        final Iterable<T> tmp = getChildren(childClass);
        final Iterator<T> iter = tmp.iterator();
        if (iter.hasNext()) {
            return iter.next();
        } else {
            return null;
        }
    }

    public final <T extends RdbElement> boolean hasChildren(Class<T> childClass,
                                                            String name) {
        return getFirstChild(childClass, name) != null;
    }

    @Override
    public String toString() {
        return getKind() + " '" + getName() + "'";
    }

    public abstract static class Builder<B extends Builder<B>> {
        protected String name;
        private String comments;

        protected Builder() {
        }

        @SuppressWarnings("unchecked")
        public B self() {
            return (B) this;
        }

        public B name(String name) {
            this.name = name;
            return self();
        }

        public B comments(String comments) {
            this.comments = comments;
            return self();
        }

        public abstract RdbElement build();
    }
}