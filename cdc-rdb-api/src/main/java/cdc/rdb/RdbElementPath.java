package cdc.rdb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import cdc.util.lang.Checks;

/**
 * Path associated to an element.
 * <p>
 * For elements that can have siblings with same name (e.g., data type, function or procedure),
 * a Path, as currently defined, can be ambiguous.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbElementPath implements Comparable<RdbElementPath> {
    private final Part[] parts;

    public static final char KIND_SEPARATOR = ':';
    public static final char PART_SEPARATOR = '/';

    public static final class Part {
        private final Class<? extends RdbElement> elementClass;
        private final String name;

        Part(Class<? extends RdbElement> elementClass,
             String name) {
            this.elementClass = elementClass;
            this.name = name;
        }

        Part(RdbElement element) {
            this(element.getClass(),
                 element.getName());
        }

        public Class<? extends RdbElement> getElementClass() {
            return elementClass;
        }

        public String getName() {
            return name;
        }

        @Override
        public int hashCode() {
            return Objects.hash(elementClass,
                                name);
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Part)) {
                return false;
            }
            final Part other = (Part) object;
            return elementClass == other.elementClass
                    && name.equals(other.name);
        }

        @Override
        public String toString() {
            return "[" + RdbElement.getKind(elementClass) + ":" + name + "]";
        }
    }

    RdbElementPath(RdbElement element) {
        this.parts = new Part[element.getDepth()];
        int index = parts.length;
        RdbElement iter = element;
        while (iter != null) {
            index--;
            this.parts[index] = new Part(iter);
            iter = iter.getParent();
        }
    }

    private RdbElementPath(RdbElementPath path,
                           int length) {
        this.parts = Arrays.copyOf(path.parts, length);
    }

    public RdbElementPath(Class<? extends RdbElement> tailClass,
                          String s) {
        Checks.isNotNull(tailClass, "tailClass");
        Checks.isNotNull(s, "s");

        // Extract names
        final List<String> names = new ArrayList<>();
        int from = 0;
        // Number of part separators
        int seps = 0;
        while (from < s.length()) {
            final int to = s.indexOf(PART_SEPARATOR, from);
            if (to < 0) {
                names.add(s.substring(from));
                from = s.length();
            } else {
                names.add(s.substring(from, to));
                from = to + 1;
                seps++;
            }
        }
        if (seps == names.size()) {
            names.add("");
        }

        // Build parts
        if (RdbElement.getDepth(tailClass) == names.size()) {
            this.parts = new Part[names.size()];
            Class<? extends RdbElement> k = tailClass;
            for (int index = parts.length - 1; index >= 0; index--) {
                final Part part = new Part(k, names.get(index));
                this.parts[index] = part;
                k = RdbElement.getParentClass(k);
            }
        } else {
            throw new IllegalArgumentException("Invalid number of parts " + names.size()
                    + " for " + tailClass
                    + ", expecting " + RdbElement.getDepth(tailClass) + " in '" + s + "'");
        }
    }

    public RdbElementPath(String s) {
        this(getTailClass(s),
             getPath(s));
    }

    private static Class<? extends RdbElement> getTailClass(String s) {
        Checks.isNotNull(s, "s");
        final int pos = s.indexOf(KIND_SEPARATOR);
        if (pos < 0) {
            throw new IllegalArgumentException("Invalid path '" + s + "', missing '" + KIND_SEPARATOR + "'");
        }
        try {
            return RdbElement.getElementClass(s.substring(0, pos));
        } catch (final IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid path '" + s + "', can not decode '" + s.substring(0, pos) + "'");
        }
    }

    private static String getPath(String s) {
        Checks.isNotNull(s, "s");
        final int pos = s.indexOf(KIND_SEPARATOR);
        if (pos < 0) {
            throw new IllegalArgumentException("Invalid path '" + s + "', missing '" + KIND_SEPARATOR + "'");
        } else {
            return s.substring(pos + 1);
        }
    }

    public int getLength() {
        return parts.length;
    }

    public Part[] getParts() {
        return parts;
    }

    public Part getPart(int index) {
        return parts[index];
    }

    public Class<? extends RdbElement> getTailClass() {
        return parts[parts.length - 1].getElementClass();
    }

    public boolean hasPart(Class<? extends RdbElement> elementClass) {
        Checks.isNotNull(elementClass, "kind");

        final int index = RdbElement.getDepth(elementClass) - 1;
        return index < parts.length && parts[index].getElementClass() == elementClass;
    }

    public Part getPart(Class<? extends RdbElement> elementClass) {
        Checks.isNotNull(elementClass, "elementClass");

        final int index = RdbElement.getDepth(elementClass) - 1;
        final Part part = index < parts.length ? parts[index] : null;
        return part != null && part.getElementClass() == elementClass ? part : null;
    }

    public RdbElementPath getParent() {
        if (getLength() == 1) {
            return null;
        } else {
            return new RdbElementPath(this, getLength() - 1);
        }
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(parts);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof RdbElementPath)) {
            return false;
        }
        final RdbElementPath other = (RdbElementPath) object;
        return Arrays.equals(parts, other.parts);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(RdbElement.getKind(getTailClass()));
        builder.append(KIND_SEPARATOR);
        for (int index = 0; index < getLength(); index++) {
            if (index > 0) {
                builder.append(PART_SEPARATOR);
            }
            builder.append(getPart(index).getName());
        }
        return builder.toString();
    }

    @Override
    public int compareTo(RdbElementPath other) {
        return toString().compareTo(other.toString());
    }
}