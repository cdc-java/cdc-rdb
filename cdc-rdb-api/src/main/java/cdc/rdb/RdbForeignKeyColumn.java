package cdc.rdb;

/**
 * Foreign key column description.
 * <p>
 * Its parent is a Foreign key.<br>
 * Its name must be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbForeignKeyColumn extends RdbChildElement<RdbForeignKey> {
    public static final String KIND = KIND_FOREIGN_KEY_COLUMN;

    private final short ordinal;
    private final String refColumnName;

    private RdbForeignKeyColumn(Builder builder) {
        super(builder, false);
        this.ordinal = builder.ordinal;
        this.refColumnName = builder.refColumnName;
    }

    public short getOrdinal() {
        return ordinal;
    }

    public String getRefColumnName() {
        return refColumnName;
    }

    public RdbPrimaryKeyColumn getRefColumn() {
        final RdbTable refTable = getParent().getRefTable();
        return refTable == null ? null : refTable.getOptionalPrimaryKey().getOptionalColumn(refColumnName);
    }

    static Builder builder(RdbForeignKey parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbForeignKey> {
        private short ordinal;
        private String refColumnName;

        private Builder(RdbForeignKey parent) {
            super(parent);
        }

        public Builder ordinal(short ordinal) {
            this.ordinal = ordinal;
            return self();
        }

        public Builder refColumnName(String refColumnName) {
            this.refColumnName = refColumnName;
            return self();
        }

        @Override
        public RdbForeignKeyColumn build() {
            return new RdbForeignKeyColumn(this);
        }
    }
}