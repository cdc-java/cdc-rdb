package cdc.rdb;

/**
 * Function column description.
 * <p>
 * Its parent is a Function.<br>
 * Its name must be unique.(?)<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbFunctionColumn extends RdbChildElement<RdbFunction> {
    public static final String KIND = KIND_FUNCTION_COLUMN;

    // TODO attributes
    protected RdbFunctionColumn(Builder builder) {
        super(builder, false);
    }

    public RdbFunction getFunction() {
        return getParent();
    }

    static Builder builder(RdbFunction parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbFunction> {
        private Builder(RdbFunction parent) {
            super(parent);
        }

        @Override
        public RdbFunctionColumn build() {
            return new RdbFunctionColumn(this);
        }
    }
}