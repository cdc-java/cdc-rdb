package cdc.rdb;

/**
 * Foreign key description.
 * <p>
 * Its parent is a Table.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Foreign key columns
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbForeignKey extends RdbChildElement<RdbTable> {
    public static final String KIND = KIND_FOREIGN_KEY;

    private final String refCatalogName;
    private final String refSchemaName;
    private final String refTableName;

    private RdbForeignKey(Builder builder) {
        super(builder, false);
        this.refCatalogName = builder.refCatalogName;
        this.refSchemaName = builder.refSchemaName;
        this.refTableName = builder.refTableName;
    }

    public RdbForeignKeyColumn.Builder column() {
        return RdbForeignKeyColumn.builder(this);
    }

    public Iterable<RdbForeignKeyColumn> getColumns() {
        return getChildren(RdbForeignKeyColumn.class);
    }

    public String getRefCatalogName() {
        return refCatalogName;
    }

    public RdbCatalog getRefCatalog() {
        return getParent().getDatabase().getOptionalCatalog(refCatalogName);
    }

    public String getRefSchemaName() {
        return refSchemaName;
    }

    public RdbSchema getRefSchema() {
        final RdbCatalog refCatalog = getRefCatalog();
        return refCatalog == null ? null : refCatalog.getOptionalSchema(refSchemaName);
    }

    public String getRefTableName() {
        return refTableName;
    }

    public RdbTable getRefTable() {
        final RdbSchema refSchema = getRefSchema();
        return refSchema == null ? null : refSchema.getOptionalTable(refTableName);
    }

    static Builder builder(RdbTable parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbTable> {
        private String refCatalogName;
        private String refSchemaName;
        private String refTableName;

        private Builder(RdbTable parent) {
            super(parent);
        }

        public Builder refCatalogName(String refCatalogName) {
            this.refCatalogName = refCatalogName;
            return self();
        }

        public Builder refSchemaName(String refSchemaName) {
            this.refSchemaName = refSchemaName;
            return self();
        }

        public Builder refTableName(String refTableName) {
            this.refTableName = refTableName;
            return self();
        }

        @Override
        public RdbForeignKey build() {
            return new RdbForeignKey(this);
        }
    }
}