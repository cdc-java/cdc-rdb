package cdc.rdb;

/**
 * Function description.
 * <p>
 * Its parent is a Schema.<br>
 * Its name must NOT be unique.<br>
 * Its content is:
 * <ul>
 * <li>Function columns
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbFunction extends RdbChildElement<RdbSchema> {
    public static final String KIND = KIND_FUNCTION;

    private final String specificName;
    private final FunctionResultType resultType;

    RdbFunction(Builder builder) {
        super(builder, true);
        this.specificName = builder.specificName;
        this.resultType = builder.resultType;
    }

    public RdbFunctionColumn.Builder column() {
        return RdbFunctionColumn.builder(this);
    }

    public Iterable<RdbFunctionColumn> getColumns() {
        return getChildren(RdbFunctionColumn.class);
    }

    public RdbFunctionColumn getOptionalColumn(String name) {
        return getFirstChild(RdbFunctionColumn.class, name);
    }

    public RdbFunctionColumn getColumn(String name) {
        return notNull(getOptionalColumn(name), "function column", name);
    }

    public String getSpecificName() {
        return specificName;
    }

    public FunctionResultType getResultType() {
        return resultType;
    }

    static Builder builder(RdbSchema parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbSchema> {
        private String specificName;
        private FunctionResultType resultType;

        private Builder(RdbSchema parent) {
            super(parent);
        }

        public Builder specificName(String specificName) {
            this.specificName = specificName;
            return self();
        }

        public Builder resultType(FunctionResultType resultType) {
            this.resultType = resultType;
            return self();
        }

        @Override
        public RdbFunction build() {
            return new RdbFunction(this);
        }
    }
}