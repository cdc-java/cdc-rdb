package cdc.rdb;

import java.sql.DatabaseMetaData;

public enum FunctionResultType {
    UNKNOWN,
    TABLE,
    NO_TABLE;

    public static FunctionResultType decode(short code) {
        switch (code) {
        case DatabaseMetaData.functionResultUnknown:
            return UNKNOWN;
        case DatabaseMetaData.functionNoTable:
            return NO_TABLE;
        case DatabaseMetaData.functionReturnsTable:
            return TABLE;
        default:
            return null;
        }
    }
}