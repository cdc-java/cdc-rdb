package cdc.rdb;

import cdc.util.lang.UnexpectedValueException;

public class RdbIdentifierNormalizer {
    private final Style normalStyle;
    private final Style quotedStyle;
    private final char quote;
    private final String quoteString;

    public enum Style {
        LOWER_CASE,
        MIXED_CASE,
        UPPER_CASE
    }

    public RdbIdentifierNormalizer(Style normalStyle,
                                   Style quotedStyle,
                                   char quote) {
        this.normalStyle = normalStyle;
        this.quotedStyle = quotedStyle;
        this.quote = quote;
        this.quoteString = Character.toString(quote);
    }

    public static RdbIdentifierNormalizer create(RdbDatabase database) {
        if (!database.isDefined(RdbDatabase.BooleanProperty.SUPPORTS_MIXED_CASE_IDENTIFIERS)
                || !database.isDefined(RdbDatabase.BooleanProperty.SUPPORTS_MIXED_CASE_QUOTED_IDENTIFIERS)
                || !database.isDefined(RdbDatabase.BooleanProperty.STORES_LOWER_CASE_IDENTIFIERS)
                || !database.isDefined(RdbDatabase.BooleanProperty.STORES_MIXED_CASE_IDENTIFIERS)
                || !database.isDefined(RdbDatabase.BooleanProperty.STORES_UPPER_CASE_IDENTIFIERS)
                || !database.isDefined(RdbDatabase.BooleanProperty.STORES_LOWER_CASE_QUOTED_IDENTIFIERS)
                || !database.isDefined(RdbDatabase.BooleanProperty.STORES_MIXED_CASE_QUOTED_IDENTIFIERS)
                || !database.isDefined(RdbDatabase.BooleanProperty.STORES_UPPER_CASE_QUOTED_IDENTIFIERS)
                || !database.isDefined(RdbDatabase.StringProperty.IDENTIFIER_QUOTE_STRING)) {
            return null;
        }

        final boolean supportsNormalMixed = database.getProperty(RdbDatabase.BooleanProperty.SUPPORTS_MIXED_CASE_IDENTIFIERS);
        final boolean supportsQuotedMixed = database.getProperty(RdbDatabase.BooleanProperty.SUPPORTS_MIXED_CASE_QUOTED_IDENTIFIERS);

        final String quoteString = database.getProperty(RdbDatabase.StringProperty.IDENTIFIER_QUOTE_STRING);

        final Style normalStyle;
        if (supportsNormalMixed) {
            normalStyle = Style.MIXED_CASE;
        } else {
            final boolean storesNormalLowerCase = database.getProperty(RdbDatabase.BooleanProperty.STORES_LOWER_CASE_IDENTIFIERS);
            // final boolean storesNormalMixedCase = database.getProperty(RdbDatabase.BooleanProperty.STORES_MIXED_CASE_IDENTIFIERS);
            final boolean storesNormalUpperCase = database.getProperty(RdbDatabase.BooleanProperty.STORES_UPPER_CASE_IDENTIFIERS);
            if (storesNormalLowerCase) {
                normalStyle = Style.LOWER_CASE;
            } else if (storesNormalUpperCase) {
                normalStyle = Style.UPPER_CASE;
            } else {
                normalStyle = Style.MIXED_CASE;
            }
        }
        final Style quotedStyle;
        if (supportsQuotedMixed) {
            quotedStyle = Style.MIXED_CASE;
        } else {
            final boolean storesQuotedLowerCase = database.getProperty(RdbDatabase.BooleanProperty.STORES_LOWER_CASE_QUOTED_IDENTIFIERS);
            // final boolean storesQuotedMixedCase = database.getProperty(RdbDatabase.BooleanProperty.STORES_MIXED_CASE_QUOTED_IDENTIFIERS);
            final boolean storesQuotedUpperCase = database.getProperty(RdbDatabase.BooleanProperty.STORES_UPPER_CASE_QUOTED_IDENTIFIERS);
            if (storesQuotedLowerCase) {
                quotedStyle = Style.LOWER_CASE;
            } else if (storesQuotedUpperCase) {
                quotedStyle = Style.UPPER_CASE;
            } else {
                quotedStyle = Style.MIXED_CASE;
            }
        }

        return new RdbIdentifierNormalizer(normalStyle, quotedStyle, quoteString.charAt(0));
    }

    public Style getNormalStyle() {
        return normalStyle;
    }

    public Style getQuotedStyle() {
        return quotedStyle;
    }

    public char getQuote() {
        return quote;
    }

    public boolean isQuoted(String id) {
        return id.startsWith(quoteString) && id.endsWith(quoteString);
    }

    public String normalize(String id) {
        if (isQuoted(id)) {
            // Assume quote char is not case sensitive.
            switch (quotedStyle) {
            case LOWER_CASE:
                return id.toLowerCase();
            case MIXED_CASE:
                return id;
            case UPPER_CASE:
                return id.toUpperCase();
            default:
                throw new UnexpectedValueException(normalStyle);
            }
        } else {
            switch (normalStyle) {
            case LOWER_CASE:
                return id.toLowerCase();
            case MIXED_CASE:
                return id;
            case UPPER_CASE:
                return id.toUpperCase();
            default:
                throw new UnexpectedValueException(normalStyle);
            }
        }
    }
}