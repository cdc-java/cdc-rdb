package cdc.rdb;

/**
 * User Data type description.
 * <p>
 * Its parent is a Schema.<br>
 * Its name must be unique.(?)<br>
 * Its content is:
 * <ul>
 * <li>Attributes
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbUserDataType extends RdbChildElement<RdbSchema> {
    public static final String KIND = KIND_USER_DATA_TYPE;

    private final String className;
    private final SqlDataType type;

    private RdbUserDataType(Builder builder) {
        super(builder, false);
        this.className = builder.className;
        this.type = builder.type;
    }

    public String getClassName() {
        return className;
    }

    public SqlDataType getType() {
        return type;
    }

    public RdbAttribute.Builder attribute() {
        return RdbAttribute.builder(this);
    }

    public Iterable<RdbAttribute> getAttributes() {
        return getChildren(RdbAttribute.class);
    }

    public RdbAttribute getOptionalAttribute(String name) {
        return getFirstChild(RdbAttribute.class, name);
    }

    public RdbAttribute getAttribute(String name) {
        return notNull(getOptionalAttribute(name), "attribute", name);
    }

    static Builder builder(RdbSchema parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbSchema> {
        private String className;
        private SqlDataType type;

        protected Builder(RdbSchema parent) {
            super(parent);
        }

        public Builder className(String className) {
            this.className = className;
            return self();
        }

        public Builder type(SqlDataType type) {
            this.type = type;
            return self();
        }

        @Override
        public RdbUserDataType build() {
            return new RdbUserDataType(this);
        }
    }
}