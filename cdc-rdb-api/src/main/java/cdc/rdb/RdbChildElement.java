package cdc.rdb;

import cdc.util.lang.Checks;

public abstract class RdbChildElement<P extends RdbElement> extends RdbElement {
    private final P parent;
    private final RdbElementPath path;

    protected RdbChildElement(Builder<?, P> builder,
                              boolean allowDuplicateNames) {
        super(builder);
        this.parent = Checks.isNotNull(builder.parent, "parent");

        if (!allowDuplicateNames && parent.hasChildren(getClass(), builder.name)) {
            throw new IllegalArgumentException("A sibling named '" + builder.name + "' and type " + getClass().getSimpleName()
                    + " already exists");
        }
        this.parent.children.add(this);
        this.path = new RdbElementPath(this);
    }

    @Override
    public final P getParent() {
        return parent;
    }

    @Override
    public final RdbElementPath getPath() {
        return path;
    }

    public abstract static class Builder<B extends Builder<B, P>, P extends RdbElement> extends RdbElement.Builder<B> {
        protected final P parent;

        protected Builder(P parent) {
            this.parent = parent;
        }
    }
}