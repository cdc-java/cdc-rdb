package cdc.rdb;

/**
 * Catalog description.
 * <p>
 * Its parent is a Database.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Schemas
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbCatalog extends RdbChildElement<RdbDatabase> {
    public static final String KIND = KIND_CATALOG;

    RdbCatalog(Builder builder) {
        super(builder, false);
    }

    public RdbDatabase getDatabase() {
        return getParent();
    }

    public RdbSchema.Builder schema() {
        return RdbSchema.builder(this);
    }

    public RdbSchema getOptionalSchema(String name) {
        return getFirstChild(RdbSchema.class, name);
    }

    public RdbSchema getSchema(String name) {
        return notNull(getOptionalSchema(name), "schema", name);
    }

    public RdbSchema getOrCreateSchema(String name) {
        RdbSchema result = getOptionalSchema(name);
        if (result == null) {
            result = schema().name(name).build();
        }
        return result;
    }

    public Iterable<RdbSchema> getSchemas() {
        return getChildren(RdbSchema.class);
    }

    static Builder builder(RdbDatabase parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbDatabase> {
        private Builder(RdbDatabase parent) {
            super(parent);
        }

        @Override
        public RdbCatalog build() {
            return new RdbCatalog(this);
        }
    }
}