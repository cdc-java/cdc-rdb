package cdc.rdb;

/**
 * Procedure column description.
 * <p>
 * Its parent is a Procedure.<br>
 * Its name must be unique. (? A duplicate has been found in PostgreSQL)<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbProcedureColumn extends RdbChildElement<RdbProcedure> {
    public static final String KIND = KIND_PROCEDURE_COLUMN;

    // TODO attributes
    protected RdbProcedureColumn(Builder builder) {
        super(builder, false);
    }

    public RdbProcedure getProcedure() {
        return getParent();
    }

    static Builder builder(RdbProcedure parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbProcedure> {
        private Builder(RdbProcedure parent) {
            super(parent);
        }

        @Override
        public RdbProcedureColumn build() {
            return new RdbProcedureColumn(this);
        }
    }
}