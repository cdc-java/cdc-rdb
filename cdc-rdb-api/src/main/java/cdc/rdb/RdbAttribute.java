package cdc.rdb;

/**
 * Attribute description.
 * <p>
 * Its parent is a User data type.<br>
 * Its name must be unique.(?)<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbAttribute extends RdbChildElement<RdbUserDataType> {
    public static final String KIND = KIND_ATTRIBUTE;

    // TODO attributes
    private RdbAttribute(Builder builder) {
        super(builder, false);
    }

    public RdbUserDataType getUserDataType() {
        return getParent();
    }

    static Builder builder(RdbUserDataType parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbUserDataType> {
        protected Builder(RdbUserDataType parent) {
            super(parent);
        }

        @Override
        public RdbAttribute build() {
            return new RdbAttribute(this);
        }
    }
}