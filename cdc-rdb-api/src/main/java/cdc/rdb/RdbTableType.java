package cdc.rdb;

/**
 * Table description.
 * <p>
 * Its parent is a Database.<br>
 * Its name must be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbTableType extends RdbChildElement<RdbDatabase> {
    public static final String KIND = KIND_TABLE_TYPE;

    RdbTableType(Builder builder) {
        super(builder, false);
    }

    static Builder builder(RdbDatabase parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbDatabase> {
        protected Builder(RdbDatabase parent) {
            super(parent);
        }

        @Override
        public RdbTableType build() {
            return new RdbTableType(this);
        }
    }
}