package cdc.rdb;

/**
 * Index description.
 * <p>
 * Its parent is a Table.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Index columns
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbIndex extends RdbChildElement<RdbTable> {
    public static final String KIND = KIND_INDEX;

    private final RdbIndexType type;

    protected RdbIndex(Builder builder) {
        super(builder, false);
        this.type = builder.type;
    }

    public RdbTable getTable() {
        return getParent();
    }

    public RdbIndexColumn.Builder column() {
        return RdbIndexColumn.builder(this);
    }

    public Iterable<RdbIndexColumn> getColumns() {
        return getChildren(RdbIndexColumn.class);
    }

    public RdbIndexColumn getOptionalColumn(String name) {
        return getFirstChild(RdbIndexColumn.class, name);
    }

    public RdbIndexColumn getColumn(String name) {
        return notNull(getOptionalColumn(name), "index column", name);
    }

    public RdbIndexType getType() {
        return type;
    }

    static Builder builder(RdbTable parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbTable> {
        private RdbIndexType type;

        private Builder(RdbTable parent) {
            super(parent);
        }

        public Builder type(RdbIndexType type) {
            this.type = type;
            return self();
        }

        @Override
        public RdbIndex build() {
            return new RdbIndex(this);
        }
    }
}