package cdc.rdb;

/**
 * Table description.
 * <p>
 * Its parent is a Schema.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Table columns
 * <li>Primary key
 * <li>Foreign keys
 * <li>Indices
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbTable extends RdbChildElement<RdbSchema> {
    public static final String KIND = KIND_TABLE;

    private final String tableTypeName;

    private RdbTable(Builder builder) {
        super(builder, false);
        this.tableTypeName = builder.tableTypeName;
    }

    public RdbSchema getSchema() {
        return getParent();
    }

    public RdbCatalog getCatalog() {
        return getSchema().getCatalog();
    }

    public RdbDatabase getDatabase() {
        return getCatalog().getDatabase();
    }

    public String getTableTypeName() {
        return tableTypeName;
    }

    public RdbTableType getTableType() {
        return getDatabase().getOptionalTableType(tableTypeName);
    }

    // Columns
    public RdbTableColumn.Builder column() {
        return RdbTableColumn.builder(this);
    }

    public RdbTableColumn getOptionalColumn(String name) {
        return getFirstChild(RdbTableColumn.class, name);
    }

    public RdbTableColumn getColumn(String name) {
        return notNull(getOptionalColumn(name), "table column", name);
    }

    public Iterable<RdbTableColumn> getColumns() {
        return getChildren(RdbTableColumn.class);
    }

    // PK

    public RdbPrimaryKey.Builder primaryKey() {
        return RdbPrimaryKey.builder(this);
    }

    public RdbPrimaryKey getOptionalPrimaryKey() {
        return getFirstChild(RdbPrimaryKey.class);
    }

    public RdbPrimaryKey getPrimaryKey() {
        return notNull(getOptionalPrimaryKey(), "primary key", "");
    }

    public RdbPrimaryKey getOrCreatePrimaryKey(String name) {
        RdbPrimaryKey result = getOptionalPrimaryKey();
        if (result == null) {
            result = primaryKey().name(name).build();
        }
        return result;
    }

    // FK

    public RdbForeignKey.Builder foreignKey() {
        return RdbForeignKey.builder(this);
    }

    public RdbForeignKey getOptionalForeignKey(String name) {
        return getFirstChild(RdbForeignKey.class, name);
    }

    public RdbForeignKey getForeignKey(String name) {
        return notNull(getOptionalForeignKey(name), "foreign key", name);
    }

    public RdbForeignKey getOrCreateForeignKey(String name) {
        RdbForeignKey result = getOptionalForeignKey(name);
        if (result == null) {
            result = foreignKey().name(name).build();
        }
        return result;
    }

    public Iterable<RdbForeignKey> getForeignKeys() {
        return getChildren(RdbForeignKey.class);
    }

    // Indices

    public RdbIndex.Builder index() {
        return RdbIndex.builder(this);
    }

    public RdbIndex getOptionalIndex(String name) {
        return getFirstChild(RdbIndex.class, name);
    }

    public RdbIndex getIndex(String name) {
        return notNull(getOptionalIndex(name), "index", name);
    }

    public RdbIndex getOrCreateIndex(String name) {
        RdbIndex result = getOptionalIndex(name);
        if (result == null) {
            result = index().name(name).build();
        }
        return result;
    }

    public Iterable<RdbIndex> getIndices() {
        return getChildren(RdbIndex.class);
    }

    static Builder builder(RdbSchema parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbSchema> {
        private String tableTypeName;

        protected Builder(RdbSchema parent) {
            super(parent);
        }

        public Builder tableTypeName(String tableTypeName) {
            this.tableTypeName = tableTypeName;
            return self();
        }

        @Override
        public RdbTable build() {
            return new RdbTable(this);
        }
    }

}