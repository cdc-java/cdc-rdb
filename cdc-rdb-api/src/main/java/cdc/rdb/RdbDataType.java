package cdc.rdb;

/**
 * Data type description.
 * <p>
 * Its parent is a Database.<br>
 * Its name must NOT be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbDataType extends RdbChildElement<RdbDatabase> {
    public static final String KIND = KIND_DATA_TYPE;

    private final SqlDataType type;
    private final int maxPrecision;
    private final String literalPrefix;
    private final String literalSuffix;
    private final String createParams;
    private final YesNoUnknown nullable;
    private final boolean caseSensitive;
    private final boolean unsigned;
    private final boolean fixedPrecisionScale;
    private final String localizedName;
    private final boolean autoIncrement;
    private final short minScale;
    private final short maxScale;
    private final int radix;

    RdbDataType(Builder builder) {
        super(builder, true);
        this.type = builder.type;
        this.maxPrecision = builder.maxPrecision;
        this.literalPrefix = builder.literalPrefix;
        this.literalSuffix = builder.literalSuffix;
        this.createParams = builder.createParams;
        this.nullable = builder.nullable;
        this.caseSensitive = builder.caseSensitive;
        this.unsigned = builder.unsigned;
        this.fixedPrecisionScale = builder.fixedPrecisionScale;
        this.localizedName = builder.localizedName;
        this.autoIncrement = builder.autoIncrement;
        this.minScale = builder.minScale;
        this.maxScale = builder.maxScale;
        this.radix = builder.radix;
    }

    public SqlDataType getType() {
        return type;
    }

    public int getMaxPrecision() {
        return maxPrecision;
    }

    public String getLiteralPrefix() {
        return literalPrefix;
    }

    public String getLiteralSuffix() {
        return literalSuffix;
    }

    public String getCreateParams() {
        return createParams;
    }

    public YesNoUnknown getNullable() {
        return nullable;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public boolean isUnsigned() {
        return unsigned;
    }

    public boolean isFixedPrecisionScale() {
        return fixedPrecisionScale;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    public short getMinScale() {
        return minScale;
    }

    public short getMaxScale() {
        return maxScale;
    }

    public int getRadix() {
        return radix;
    }

    static Builder builder(RdbDatabase parent) {
        return new Builder(parent);
    }

    public static final class Builder extends RdbChildElement.Builder<Builder, RdbDatabase> {
        private SqlDataType type;
        private int maxPrecision = -1;
        private String literalPrefix;
        private String literalSuffix;
        private String createParams;
        private YesNoUnknown nullable;
        private boolean caseSensitive;
        private boolean unsigned;
        private boolean fixedPrecisionScale;
        private String localizedName;
        private boolean autoIncrement;
        private short minScale;
        private short maxScale;
        private int radix;

        protected Builder(RdbDatabase parent) {
            super(parent);
        }

        public Builder type(SqlDataType type) {
            this.type = type;
            return self();
        }

        public Builder maxPrecision(int maxPrecision) {
            this.maxPrecision = maxPrecision;
            return self();
        }

        public Builder literalPrefix(String literalPrefix) {
            this.literalPrefix = literalPrefix;
            return self();
        }

        public Builder literalSuffix(String literalSuffix) {
            this.literalSuffix = literalSuffix;
            return self();
        }

        public Builder createParams(String createParams) {
            this.createParams = createParams;
            return self();
        }

        public Builder nullable(YesNoUnknown nullable) {
            this.nullable = nullable;
            return self();
        }

        public Builder caseSensitive(boolean caseSensitive) {
            this.caseSensitive = caseSensitive;
            return self();
        }

        public Builder unsigned(boolean unsigned) {
            this.unsigned = unsigned;
            return self();
        }

        public Builder fixedPrecisionScale(boolean fixedPrecisionScale) {
            this.fixedPrecisionScale = fixedPrecisionScale;
            return self();
        }

        public Builder localizedName(String localizedName) {
            this.localizedName = localizedName;
            return self();
        }

        public Builder autoIncrement(boolean autoIncrement) {
            this.autoIncrement = autoIncrement;
            return self();
        }

        public Builder minScale(short minScale) {
            this.minScale = minScale;
            return self();
        }

        public Builder maxScale(short maxScale) {
            this.maxScale = maxScale;
            return self();
        }

        public Builder radix(int radix) {
            this.radix = radix;
            return self();
        }

        @Override
        public RdbDataType build() {
            return new RdbDataType(this);
        }
    }
}