package cdc.rdb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

class RdbElementPathTest {
    private static void testParts(RdbElementPath path) {
        final Set<Class<? extends RdbElement>> kinds = new HashSet<>();
        Class<? extends RdbElement> cls = path.getTailClass();
        while (cls != null) {
            kinds.add(cls);
            cls = RdbElement.getParentClass(cls);
        }
        for (final Class<? extends RdbElement> k : RdbElement.getElementClasses()) {
            assertEquals(kinds.contains(k), path.hasPart(k));
            if (kinds.contains(k)) {
                assertEquals(k, path.getPart(k).getElementClass());
            } else {
                assertEquals(null, path.getPart(k));
            }
        }
    }

    private static void testConstructor(String s,
                                        Class<? extends RdbElement> expectedTailClass) {
        final RdbElementPath path = new RdbElementPath(s);
        assertEquals(expectedTailClass, path.getTailClass());
        assertEquals(s, path.toString());
        final Class<? extends RdbElement> expectedParentClass = RdbElement.getParentClass(expectedTailClass);
        final RdbElementPath parent = path.getParent();
        final Class<? extends RdbElement> parentTailClass = parent == null ? null : parent.getTailClass();
        assertEquals(expectedParentClass, parentTailClass);
        testParts(path);
    }

    @Test
    void testConstructor() {
        testConstructor("DATABASE:db", RdbDatabase.class);
        testConstructor("DATABASE:", RdbDatabase.class);

        testConstructor("DATA_TYPE:db/dt", RdbDataType.class);
        testConstructor("DATA_TYPE:db/", RdbDataType.class);
        testConstructor("DATA_TYPE:/dt", RdbDataType.class);
        testConstructor("DATA_TYPE:/", RdbDataType.class);

        testConstructor("TABLE_TYPE:db/tt", RdbTableType.class);
        testConstructor("TABLE_TYPE:db/", RdbTableType.class);
        testConstructor("TABLE_TYPE:/tt", RdbTableType.class);
        testConstructor("TABLE_TYPE:/", RdbTableType.class);

        testConstructor("CATALOG:db/cat", RdbCatalog.class);
        testConstructor("CATALOG:db/", RdbCatalog.class);
        testConstructor("CATALOG:/cat", RdbCatalog.class);
        testConstructor("CATALOG:/", RdbCatalog.class);

        testConstructor("SCHEMA:db/cat/schema", RdbSchema.class);

        testConstructor("USER_DATA_TYPE:db/cat/schema/udt", RdbUserDataType.class);
        testConstructor("FUNCTION:db/cat/schema/fun", RdbFunction.class);
        testConstructor("PROCEDURE:db/cat/schema/proc", RdbProcedure.class);
        testConstructor("TABLE:db/cat/schema/table", RdbTable.class);
    }

    @Test
    void testInvalidPath1() {
        assertThrows(IllegalArgumentException.class, () -> {
            new RdbElementPath("ABLE:db/cat/schema/table");
        });
    }

    @Test
    void testInvalidPath2() {
        assertThrows(IllegalArgumentException.class, () -> {
            new RdbElementPath("TABLEdb/cat/schema/table");
        });
    }

    @Test
    void testInvalidPath3() {
        assertThrows(IllegalArgumentException.class, () -> {
            new RdbElementPath("TABLE:db/cat/schema/table/column");
        });
    }
}