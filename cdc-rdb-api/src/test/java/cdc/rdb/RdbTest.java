package cdc.rdb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class RdbTest {
    @Test
    void test1() {
        final RdbDatabase db = RdbDatabase.builder().name("db").build();
        final RdbCatalog catalog1 = db.catalog().name("catalog1").build();
        final RdbSchema schema1 = catalog1.schema().name("schema1").build();
        final RdbTable table1 = schema1.table().name("table1").build();
        final RdbTableColumn tableColumn1 = table1.column().name("column1").build();
        final RdbTableColumn tableColumn2 = table1.column().name("column2").build();

        assertEquals(db, db.getElement(db.getPath()));
        assertEquals(catalog1, db.getElement(catalog1.getPath()));
        assertEquals(schema1, db.getElement(schema1.getPath()));
        assertEquals(table1, db.getElement(table1.getPath()));
        assertEquals(tableColumn1, db.getElement(tableColumn1.getPath()));
        assertEquals(tableColumn2, db.getElement(tableColumn2.getPath()));
    }
}